=head1 NAME

ActionParser - Makepp action parser base class

=head1 SYNOPSIS

	perl_begin
	 { package ActionParser::MyParser;
	 	use vars qw/@ISA/;
		@ISA=qw/ActionParser/;
	 }
	 sub parser_myscan{
	 	return new ActionParser::MyParser;
	 };
	perl_end

	target: dependency : scanner myscan
		action

=head1 DESCRIPTION

C<ActionParser> is a base class for makepp(1) action parsers.
It is responsible for dealing with generic shell command parsing.

Ideally, rule options should indicate whether back-quotes should be expanded
and/or parsed.
Currently, they are neither.

=head1 THE SCANNER INTERFACE

When a scanner $parser is specified for a rule, first a subroutine named
"parser_$parser" is sought.
If it is found, then the parser object returned by that subroutine is used
for the rule.
Second, a subroutine named "scanner_$parser" is sought.
If it is found, then a action parser that always uses that routine as the
command parser is used.
The second subroutine search is for legacy compatibility, and it is deprecated.

Note that this is implemented in the Makefile package, but we explain it here
because you only need to know about it when you're messing with custom
scanners.

=cut

use strict;
package ActionParser;

use TextSubs;

=head1 METHODS

=head2 new

	my $parser=new ActionParser;

Returns a new ActionParser object.

=cut

my $parser;

sub new {
  my $self=shift;
  my $class=ref($self)||$self;

  # Optimize the default parser constructor, because we know it's stateless.
  if($class eq __PACKAGE__) {
	  $parser=bless {}, $class unless $parser;
	  return $parser;
  }

  bless {}, $class;
}

=head2 parse_rule

	$parser->parse_rule($actions, $rule);

For each action in $actions, first deal with shell stuff, for example:

=over 3

=item 1.

Break the actions into commands (split on C<;>, C<&>, C<&&>, C<|> and C<||>)

=item 2.

Deal with I/O redirectors [TBD]

=item 3.

Determine which environment variables are set by each action [partially done]

=item 4.

Expand and/or parse back-quoted expressions if $rule's options so indicate [TBD]

=item 5.

For "cd" commands, determine the directory into which it changes [TBD]

=back

For each remaining command, call parse_command().

$actions represents $rule's newline-separated actions, which are not derived
from the rule itself so as to avoid duplicated expansion work.

Return value is TRUE on success.

=cut

my %scanner_warnings;

sub parse_rule {
  my $self=shift;
  my ($actions, $rule)=@_;
  require Rule;
  my @actions=Rule::split_actions($actions);
  require TextSubs;
  my $parsers_found = 0;
  for my $action_rec (@actions) {
    my $action=$action_rec->[0];
    next unless defined $action;
    my $dir=".";
    my %setenv;

    # Split action into commands
    # TBD: This isn't quite right, because env and cwd settings don't propagate
    # to the next command if they are separated by '&' or '|'.
    my @commands = split_commands($action);

    for my $command (@commands) {
      # TBD: deal with '<' and add dependency
      # TBD: deal with '>' and add target
      my %env=%setenv; # copy the set env to isolate command settings
      while ($command =~ s/^\s*(\w+)=//) {
        my $var=$1;
                                # Is there an environment variable assignment?
        my $ix = TextSubs::index_ignoring_quotes($command, " ");
                                # Look for the next whitespace.
        $ix >= 0 or $ix = TextSubs::index_ignoring_quotes($command, "\t");
                                # Oops, it must be a tab.
        $ix >= 0 or last;       # Can't find the end--something's wrong.
        $env{$var}=substr($command, 0, $ix);
        $action = substr($command, $ix+1); # Chop off the environment variable.
        $action =~ s/^\s+//;
      }
      # TBD: expand and/or parse back-quotes
      # TBD: deal with cd commands and set $dir
      # TBD: deal with export (sh's rendition of setenv) commands and add
      # to %setenv
      my $result=$self->parse_command($command, $rule, $dir, \%env);
      return undef unless defined($result);
      $result and ++$parsers_found;
    }
  }

# 
# We want to try to avoid the case where because of some quirk in the command,
# we failed to find a scanner.  Scanners are especially important for 
# C/C++ compilation.  See if this rule looks like it's some sort of a 
# compile:
#     
  if ($parsers_found == 0) {    # No scanners found at all?
    my $tinfo = $rule->{EXPLICIT_TARGETS}[0];
    if (ref($tinfo) eq 'FileInfo' && # Target is a file?
        $tinfo->{NAME} =~ /\.l?o$/) { # And it's an object file?
      my $deps = $rule->{EXPLICIT_DEPENDENCIES};
      if (@$deps &&             # There is a dependency?
          ref($deps->[0]) eq 'FileInfo' && # It's a file?
          $deps->[0]{NAME} =~ /\.(?:c|cxx|c\+\+|cc|cpp)$/i) {
                                # Looks like C source code?
        if ($main::warn_level) {
          main::print_warning( 'action scanner not found for rule at `',
                               $rule->source,
                               "'\nalthough it seems to be a compilation command.
This means that makepp will not know about any include files.
To specify a scanner (and to get rid of this annoying message), add a
:scanner modifier line to the rule actions, like this:
    : scanner c_compilation     # Scans for include files.
or
    : scanner none              # Does not scan, but suppresses warning.
")
              unless $scanner_warnings{$rule->source}++;
        }
      }
    }
  }

  return 1;
}

=head2 parse_command

	$parser->parse_command($command, $rule, $dir, $setenv_hash);

Parse $command as if it is executed in directory $dir (relative to
$rule->build_cwd), and update $rule accordingly.
$setenv_hash is the environmental settings that are set by the rule itself,
with shell variables I<not> expanded.

In this base class, calls find_command_parser to determine which command
parser to use.
If the result is FALSE, then do nothing more.  (This can't
happen for an object of the base class, since we always use a
CommandParser::Basic.)
Otherwise, the resulting object's parse_command method is called, and
we return that method's return value.

Return value is TRUE on success, zero if no meaningful command parsing was
performed, and undefined if a metadependency failed to build or if scanning
failed.

=cut

sub parse_command {
  my $self=shift;
  my ($command, $rule, $dir, $setenv)=@_;
  $dir ||= ".";
  $setenv ||= {};
  my $command_parser=$self->find_command_parser($command, $rule, $dir);
  $command_parser and
    return $command_parser->parse_command($command, $setenv);
  return defined($command_parser);
}

=head2 find_command_parser

	my $command_parser=$parser->find_command_parser(
	  $command, $rule, $dir
	);

The first word of $command is looked up in %scanners from the
Makeppfile's namespace.
If it isn't found, then undef is returned (after issuing a warning if it
looks like the rule really ought to have a scanner).
Otherwise, the resulting coderef is called with the command, rule
and directory.
If the return value is a reference to an object of type CommandParser,
then it is returned.
Otherwise, 0 is returned.
Returning 0 is for backwards compatibility, and it might turn into an error
in the future.
There is no way to indicate a scanning failure if 0 is returned, for
backward compatibility.

=cut

sub find_command_parser {
  my $self=shift;
  my ($command, $rule, $dir)=@_;
  $command =~ /^\s*(\S+)\s*/;    # Get and store the first word.
  my $firstword = $1 || '';
  my $parser;
  {
    no strict 'refs';
    my $scanner_hash = \%{$rule->{MAKEFILE}{PACKAGE} . "::scanners"};
    $parser = $scanner_hash->{$firstword};
                                  # First try it unmodified.
    unless ($parser) {           # If that fails, strip out the
                                  # directory path and try again.
      $firstword =~ s@^.*/@@ and  # Is there a directory path?
        $parser ||= $scanner_hash->{$firstword};
    }
  }
  if ($parser) {               # Did we get one?
    $parser=&$parser($command, $rule, $dir) || 0;
                                # Call the routine.
    unless(UNIVERSAL::isa($parser, 'CommandParser')) {
      # This is assumed to mean that calling the %scanners value already
      # did the scanning.
      $parser=0;
      $rule->mark_scaninfo_uncacheable;
    }
  }
  else {   # No parser:
    require CommandParser::Basic;
    $parser = new CommandParser::Basic($rule, $dir);
  }
  return $parser;
}

1;
