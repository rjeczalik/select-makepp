# $Id: Signature.pm,v 1.5 2004/01/19 23:51:16 topnerd Exp $
package Signature;

=head1 NAME

Signature -- Interface definition for various signature classes

=head1 USAGE

  use Signature;		# Really!  That's all.

=head1 DESCRIPTION

Makepp is quite flexible in the algorithm it uses for deciding whether
a target is out of date with respect to its dependencies.  Most of this
flexibility is due to various different implementations of the Signature
class.

Each rule can have a different signature class associated with it,
if necessary.  In the makefile, the signature class is specified by
using the :signature modifier, like this:

   %.o : %.c
	   : signature special_build
	   $(CC) $(CFLAGS) -c $(FIRST_DEPENDENCY) -o $(TARGET)

This causes the signature class C<Signature::special_build> to be used for
this particular rule.  

Only one object from each different signature class is actually created; the
object has no data, and its only purpose is to contain a blessed reference to
the package that actually implements the functions.  Each rule contains a
reference to the Signature object that is appropriate for it.  The object is
found by the name of the Signature class.  For example, the above rule uses
the object referenced by C<$Signature::special_build::special_build>.  (The
purpose of this naming scheme is to make it impossible to inherit accidently a
singleton object, which would cause the wrong Signature class to be used.)


=head2 signature

   $signature = $sigobj->signature($objinfo);

This function returns a signature for the given object (usually a
FileInfo class, but possibly some other kind of object).  A signature is
simply an ASCII string that will change if the object is modified.

$sigobj is the dummy Signature class object.

$objinfo is the a reference to B<makepp>'s internal description of that object
and how it is to be built.  See L<makepp_extending> for details.

The default signature function simply calls $objinfo->signature, i.e.,
it uses the default signature function for objects of that class.

=cut

sub signature {
  return $_[1]->signature;
}

=head2 build_check

  if ($sigobj->build_check($tinfo, \@all_dependencies,
                           $build_command, $build_cwd, \@outdated)) {
    ...
  }

Returns undef if the given target does not need rebuilding, or the FileInfo
structure of the target that requires the rebuild.

$tinfo is the FileInfo structure for one of the targets that this rule
can make.  build_check should be called for each target.

@all_dependencies is an array that contains all dependencies (including ones
that we detected automatically, such as include files).  The dependencies
should already have been built.  The list should be sorted in the final
order we'll put them into the build information, and duplicates should be
removed.

$build_command is the command string that we are going to execute (after
all variables have been expanded) if any files are out of date.

$build_cwd is the directory that the command should be executed in if it
needs to be executed.

If \@outdated is specified, then it is appended with the list of dependencies
with respect to which the target is out of date, but only if there are no
other reasons to rebuild the target.
If the subclass doesn't support this, then it is ignored.
(This is used to avoid re-scanning unnecessarily.)

The build_check subroutine should pay attention to the ASSUME_CHANGED and
ASSUME_UNCHANGED members of the FileInfo structure.

=cut

sub build_check {
  die "Implement me";           # Implemented by classes which inherit from this.
}

1;
