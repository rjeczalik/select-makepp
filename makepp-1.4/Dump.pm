=head1 SYNOPSIS

  use Dump;
  Dump;
  Dump @makefiles;

This will Dump makepp's $FileInfo::CWD_INFO and then @makefiles to the file
.makepp_dump.pl.  Hashes are sorted sensibly, so they can be compared
before/after.

  $Dump::eliminate

is a regexp which governs the hash keys to eliminate.  Additionally '..' is
eliminated if SHORTEST_FULLNAME occurs in a hash.

=cut

use Data::Dumper;
use FileInfo;

open DUMP, '> .makepp_dump.pl';
my $oldfh = select DUMP; $| = 1; select $oldfh;
my $sep = '';

$Dump::eliminate = qr/^ENVIRONMENT$|^\.makepp/;

sub Dump::sorter($) {
  my @keys = sort keys %{$_[0]};
  @keys = grep !/$Dump::eliminate/, @keys if $Dump::eliminate;
  @keys = grep !/^\.\.$/, @keys if grep /^SHORTEST_FULLNAME$/, @keys;
  [ grep( /NAME/, @keys ), grep( !/NAME/, @keys ) ]
}

sub Dump(@) {
  local $Data::Dumper::Indent = 1;
  local $Data::Dumper::Quotekeys = 0;
  local $Data::Dumper::Sortkeys = \&Dump::sorter;
  print DUMP $sep if $sep;
  $sep = "\f\n";

  print DUMP '# ', scalar localtime, ": @{[caller]}\n";

  for (@_ ? &Dumper : Dumper $FileInfo::CWD_INFO) {
    s/\[\n +(.+)\n +\]/[ $1 ]/g;
    while (/ LSTAT => \[$/gm) {
      1 while s/\G(.*)\s+(?=[\d'\]])/$1 /gm;
    }
    s/'\n'/"\\n"/g;
    s/\n'/'."\\n"/g;
    s!^((?: {8})+)!"\t" x (length( $1 ) / 8)!gem;
    print DUMP;
  }
}

1;
