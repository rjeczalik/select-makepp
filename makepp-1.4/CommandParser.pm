=head1 NAME

CommandParser - Base class for makepp command parsers

=head1 SYNOPSIS

	use CommandParser;
	use vars qw/@ISA/;
	@ISA = qw/CommandParser/;

=head1 DESCRIPTION

C<CommandParser> is a base class for makepp(1) command parsers.

The parser for a particular command should derive from this class.
The parser should instantiate objects of type C<Scanner> for
the languageZ<>(s) of the files that it scans for include directives.

=cut

use strict;
package CommandParser;

=head1 METHODS

=head2 new

	my $parser=new CommandParser($rule, $dir);

Returns a new C<CommandParser> object for rule $rule.
$dir is the directory (a FileInfo object) in which the command runs.
The derived class may override this method and its parameters.

=cut

sub new {
	my $self=shift;
	my $class=ref($self)||$self;
	my ($rule, $dir)=@_;
	$rule && $dir or die;
	UNIVERSAL::isa($rule, 'Rule') or die;
	UNIVERSAL::isa($dir, 'FileInfo') and die;
	return bless {
	  RULE => $rule,
	  DIR => $dir,
	}, $class;
}

=head2 parse_command

	$parser->parse_command($command, $setenv_hash);

Splits the command into words, prints a log message, and calls xparse_command.

=cut

sub parse_command {
	my $self=shift;
	my ($command, $setenv_hash)=@_;
	require TextSubs;

	my @cmd_words = TextSubs::split_on_whitespace($command);

	for(@cmd_words) {
	  $_ = TextSubs::unquote($_);
	}

	if ($main::log_level) {
		my $printable_cmd = $command; 
		$printable_cmd =~ s/\n/\\n/g; # Make it fit on one line.
		main::print_log(
 "Scanning command $printable_cmd from directory " . $self->dirinfo->name .
 " for rule " . $self->rule->source
		);
	}
	$self->add_executable_dependency($cmd_words[0]);
	return $self->xparse_command(\@cmd_words, $setenv_hash);
}

=head2 add_executable_dependency

Gets called before xparse_command with the first command word as the
argument.
By default, it adds a dependency on the file given by the word, but only
if it has a relative directory name and contains no shell metacharacters.
The subclass can override this.

=cut

sub add_executable_dependency {
	my $self=shift;
	my ($exe)=@_;
	$self->add_simple_dependency($exe)
          if $exe=~m@/@ && $exe!~m@^/@ && $exe!~m|[^-+=@%^\w:,./]|;
}

=head2 xparse_command

	$parser->xparse_command($command_array, $setenv_hash);

The derived class should override this to set the default signature method,
to parse the $command_array command
and to add the implicit targets and dependencies to $self->rule.
$setenv_hash is a hashref indicating the command-line environment settings.
(Whether its values have shell variables expanded is not yet guaranteed.)

The current plan is that a new object of this class is created for each
command, but that might change in the future.
If it does, then this method is responsible for clearing previous command
information, such as the include paths.

If the command line contains another command to be parsed, then that can
be handled by calling $self->rule->scan_action recursively.

A TRUE return value indicates that some meaningful scanner was successfully
employed.
An undefined return value is interpretted as a failure.
A failure forces the rule to propagate failure status, without attempting to
build the target.

=cut

sub xparse_command {
	my $class=ref(shift);
	die "Derived class $class did not override xparse_command";
}

=head2 rule

	my $rule=$parser->rule;

Returns the rule.

=cut

sub rule {
	my $self=shift;
	return $self->{RULE};
}

=head2 dir

	my $dir=$parser->dir;

Returns the directory FileInfo object.

=cut

sub dir {
	my $self=shift;
	return $self->{DIR};
}

=head2 add_dependency

=head2 add_simple_dependency

=head2 add_target

	$parser->add_dependency($name);
	$parser->add_dependency($finfo, $tag, $src, $incname);
	$parser->add_simple_dependency($name);
	$parser->add_simple_dependency($finfo, $tag, $src, $incname);
	$parser->add_target($name);

Add a dependency or target on file $name relative to $self->dir.
Returns the FileInfo object.

The 5-arg versions add a dependency on $finfo, and record the dependency
based on $tag, $src, and $incname.
$finfo can be undefined, indicating that the file was sought but not found.
The 1-arg versions add a dependency on $name (relative to $parser->dirinfo), 
and record the dependency based on undef, undef, and $name relative to
$parser->rule->build_cwd.

add_dependency adds a "meta" dependency (a dependency on which the rule's
dependencies might depend), and causes the dependency to be built before
returning. It returns nothing if the dependency file was found, does not
yet exist and could not be built.
add_simple_dependency adds a simple dependency, and does not attempt to
build the dependency if it doesn't exist.

=cut

use FileInfo;
use FileInfo_makepp;

sub dirinfo {
	my $self = shift;
	return $self->{DIRINFO} ||=
	  file_info($self->{DIR}, $self->{RULE}->build_cwd);
}

sub relative_path {
	my ($self, $name)=@_;
	die if ref $name;
	my $base=$self->{DIR};
	die unless defined($base) && $base ne "";
	return $name if $name =~ m@^/@ || $base eq ".";
	return "$base/$name";
}

sub src_dir_name {
	my $self=shift;
	my ($src) = @_;
	my $src_dir = (
	  $src->is_or_will_be_dir ? $src : $src->{".."}
	)->name($self->{RULE}->build_cwd) if defined $src;
	return $src_dir;
}

sub add_any_dependency_ {
	my $self=shift;
	my ($meta, $name, $tag, $dir, $incname)=@_;
	my $finfo;
	if(defined $name) {
		$incname ||= $self->relative_path($name) unless ref $name;
		$finfo=file_info($name, $self->dirinfo);
#print STDERR "WAIT_FOR: @{[$finfo->absolute_filename]}\n" if $meta;
	}
	else {
		die unless defined $incname;
	}
	my $method = $meta ? "add_meta_dependency" : "add_implicit_dependency";
	$self->{RULE}->$method(
	  $tag, $self->src_dir_name($dir), $incname, $finfo
	) and $meta and return;
	return $finfo || 1;
}

sub add_dependency {
	my $self=shift;
	return $self->add_any_dependency_(1, @_);
}

# TBD: Maybe there ought to be an add_dependency_no_wait that returns a build
# handle instead of waiting for the file to be built.

sub add_simple_dependency {
	my $self=shift;
	return $self->add_any_dependency_(0, @_);
}

sub add_target {
	my $self=shift;
	my ($name)=@_;
	my $finfo=$self->{RULE}->add_implicit_target(
	  $self->relative_path($name)
	);
	return $finfo;
}

#=head1 FIELDS
#
#The following fields represent the current implementation of this class,
#but they may change without notice.
#Therefore, clients outside this class should using accessor methods instead.
#
#=head2 $self->{RULE}
#
#A reference to the rule.
#
#=head2 $self->{DIR}
#
#The directory under which the present command being parsed will execute.

=head1 FUTURE WORK

Dependencies on the environment should also be captured by xparse_command.
I don't know if there is a way to do this at all yet, but in principle it
could be added.
The $setenv_hash parameter can be used to avoid adding depedencies on
variables that are set earlier in the action.
For search paths picked up from the environment, it would be a lot more
efficient to capture dependencies in the form
"rebuild unless file X is picked up from directory Y,"
rather than "rebuild if the path changed."
Also, if we could avoid rebuilding if the file is picked up from a different
location but has the same MD5, then that would be cool.

Perhaps there should be a finfo method that just returns
file_info($_[0], $self->dirinfo).

=cut

1;
