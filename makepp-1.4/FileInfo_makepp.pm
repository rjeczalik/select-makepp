package FileInfo;

use FileInfo;			# Override some subroutines from the
				# generic FileInfo package.
# $Id: FileInfo_makepp.pm,v 1.29.2.2 2004/05/08 19:59:15 grholt Exp $

#
# This file defines some additional subroutines for the FileInfo package that
# are useful only within makepp.  This allows FileInfo.pm to be used outside
# of makepp itself.
#

use strict;


$FileInfo::build_info_subdir = ".makepp";
				# The name of the subdirectory that we store
				# build information in.

@FileInfo::build_infos_to_update = ();
				# References to all the build info files that
				# have to be flushed to disk.

push @main::end_blocks, \&update_build_infos;
				# Remember to update the build infos on exit.
				# (We don't do this explicitly from an END
				# block.  This is because cleanup of the
				# FileInfo tree can take a while, and there's
				# no need to do it if the program is just going
				# to exit, so we don't.	 This has a side effect
				# that END {} blocks aren't called.  See
				# the end of the main program.)

$FileInfo::directory_first_reference_hook = sub {};
				# This coderef is called on the first call to
				# exists_or_can_be_built with any file within
				# a given directory, with that directory's
				# FileInfo object as an arg. It should restore
				# the cwd if it changes it.

=head2 build_info_string

  my $string = $finfo->build_info_string("key");

Returns information about this file which was saved on the last build.	This
information is stored in a separate file, and is automatically invalidated
if the file it refers to has changed.  It is intended for remembering things
like the command used to build the file when it was last built, or the
signatures of the dependencies.

See also: set_build_info_string

=cut
sub build_info_string {
  my $finfo = $_[0];
  return undef unless $finfo->file_exists;

  my $binfo = $finfo->{BUILD_INFO};

  unless ($binfo) {		# We haven't loaded the build information?
    $binfo = load_build_info_file($finfo);
				# See if there's a build info file.  If we get
				# here, it wasn't available.
    $binfo ||= {};		# If we can't find any build information,
				# at least cache the failure so we don't try
				# again.
    $finfo->{BUILD_INFO} = $binfo;
  }

  return $binfo->{$_[1]};
}

=head2 get_rule

  my $rule = $finfo->get_rule;

Returns the rule to build the file, if there is one, or undef if there is none.

=cut

sub get_rule {
  return undef if $_[0]->dont_build;
  Makefile::implicitly_load($_[0]->{".."}); # Make sure we've loaded a makefile
				# for this directory.
  return $_[0]->{RULE};
}

=head2 exists_or_can_be_built

  if ($finfo->exists_or_can_be_built) { ... }

Returns true (actually, returns the FileInfo structure) if the file
exists and is readable, or does not yet exist but can be built.	 This
function determines whether a file exists by checking the build
signature, not by actually looking in the file system, so if you set
up a signature function that can return a valid build signature for a
pseudofile (like a dataset inside an HDF file or a member of an
archive) then this function will return true.

If this is not what you want, then consider the function file_exists(), which
looks in the file system to see whether the file exists or not.

=cut

sub exists_or_can_be_built_norecurse {
  my ($finfo, $phony) = @_;
  return $phony ? $finfo : 0 if $finfo->{IS_PHONY};
  return 0 if $phony;		# Never return phony targets unless requested.

  if (!$finfo->is_symbolic_link &&
      !$finfo->have_read_permission) { # File exists, but can't be read, and
				# isn't a broken symbolic link?
    return 0 if $finfo->{EXISTS}; # Can't be read--ignore it.  This is used
				# to inhibit imports from repositories.
  }

  if ($finfo->is_symbolic_link) {
    load_build_info_file($finfo); # blow away bogus repository links
  }
  return $finfo if $finfo->{EXISTS} || # We know it exists?
	$finfo->signature; 	# Has a valid signature.
  return undef;
}
sub exists_or_can_be_built {
  my ($finfo, $phony) = @_;
  my $dinfo=$finfo->{".."} || $FileInfo::root;
  unless($dinfo->{REFERENCED}++) {
    &$FileInfo::directory_first_reference_hook($dinfo);
  }
  my $result=exists_or_can_be_built_norecurse($finfo, $phony);
  return $result || undef if defined $result;
  return $finfo if
	    $finfo->{ADDITIONAL_DEPENDENCIES} ||
				# For legacy makefiles, sometimes an idiom like
				# this is used:
				#   y.tab.c: y.tab.h
				#   y.tab.h: parse.y
				#	yacc -d parse.y
				# in order to indicate that the yacc command
				# has two targets.  We need to support this
				# by indicating that files with extra
				# dependencies are buildable, even if there
				# isn't an actual rule for them.
	      $finfo->get_rule; # Rule for building it?
  # Exists in repository?
  if($finfo->{ALTERNATE_VERSIONS}) {
    for(@{$finfo->{ALTERNATE_VERSIONS}}) {
      my $result=exists_or_can_be_built_norecurse($_, $phony);
      return $result || undef if defined $result;
    }
  }
  return undef;
}

=head2 flush_build_info

  clean_fileinfos($dirinfo)

Discards all the build information for all files in the given directory
after making sure they've been written out to disk.  Also discards all
FileInfos for files which we haven't tried to build and don't have a
build rule.

=cut
sub clean_fileinfos {
#
# For some reason, the code below doesn't actually save very much memory at
# all, and it occasionally causes problems like extra rebuilds or
# forgetting about rules for some targets.  I don't understand how this
# is possible, but it happened.
#

#   my $dirinfo = $_[0];		# Get the directory.

#   &update_build_infos;		# Make sure everything's written out.
#   my ($fname, $finfo);

#   my @deletable;

#   while (($fname, $finfo) = each %{$dirinfo->{DIRCONTENTS}}) {
#				# Look at each file:
#     delete $finfo->{BUILD_INFO}; # Build info can get pretty large.
#     delete $finfo->{LSTAT};	# Toss this too, because we probably won't need
#				# it again.
#     $finfo->{DIRCONTENTS} and clean_fileinfos($finfo);
#				# Recursively clean the whole tree.
#     next if exists $finfo->{BUILD_HANDLE}; # Don't forget the files we tried to build.
#     next if $finfo->{RULE};	# Don't delete something with a rule.
#     next if $finfo->{DIRCONTENTS}; # Don't delete directories.
#     next if $finfo->{ALTERNATE_VERSIONS}; # Don't delete repository info.
#     next if $finfo->{IS_PHONY};
#     next if $finfo->{ADDITIONAL_DEPENDENCIES}; # Don't forget info about
#				# extra dependencies, either.
#     next if $finfo->{TRIGGERED_WILD}; # We can't delete it if reading it back
#				# in will trigger a wildcard routine again.
#     if ($fname eq 'all') {
#	warn("I'm deleting all now!!!\n");
#     }
#     push @deletable, $fname;	# No reason to keep this finfo structure
#				# around.  (Can't delete it, though, while
#				# we're in the middle of iterating.)
#   }
#   if (@deletable) {		# Something to delete?
#     delete @{$dirinfo->{DIRCONTENTS}}{@deletable}; # Get rid of all the unnecessary FileInfos.
#     delete $dirinfo->{READDIR};	# We might need to reread this dir.
#   }
}


=head2 move_or_link_target

  $status = $finfo->move_or_link_target($repository_file);

Links a file in from a repository or variant build cache into the current
directory.

Returns 0 if successful, nonzero if something went wrong.

=cut

$FileInfo::made_temporary_link = 0; # True if we made any temporary links.

sub move_or_link_target {
  my ($dest_finfo, $src_finfo) = @_;

  if ($dest_finfo->{DIRCONTENTS}) { # Is this a directory?
    $dest_finfo->mkdir;		# Just make it, don't soft link to it.
    return;
  }

  # Don't link to the repository if the source doesn't exist (even if it
  # can be built, because we'll build it locally instead).
  return 0 unless exists_or_can_be_built_norecurse($src_finfo);

  ++$FileInfo::made_temporary_link; # Remember to undo this temporary link.

  $dest_finfo->{".."}->mkdir;	# Make the directory (and its parents) if
				# it doesn't exist.

  $main::log_level and
    main::print_log("Linking repository file ",
		    $src_finfo->name,  " to ", $dest_finfo->name);

  # NOTE: Even if the symlink is already correct, we need to copy over the
  # build info, because it may have changed since the link was created.
  my $binfo = $src_finfo->{BUILD_INFO} ||
    load_build_info_file($src_finfo) || {};
				# Get the build information for the old file.
  my %build_info = %$binfo;	# Make a copy of everything.
  $build_info{FROM_REPOSITORY} = $src_finfo->relative_filename($dest_finfo->{".."});
				# Remember that we got it from a repository.
  $dest_finfo->{NEEDS_BUILD_UPDATE} = 1; # Haven't updated the build info.
  $dest_finfo->{BUILD_INFO} = \%build_info;
  push @FileInfo::build_infos_to_update, $dest_finfo;
				# Update it soon.

  my $changed = 1;
  if ($dest_finfo->is_symbolic_link) { # If it's already a symbolic link,
				# maybe it's correct.
    $dest_finfo->{LINK_DEREF} or $dest_finfo->dereference;
				# Get the link value.
    $dest_finfo->{LINK_DEREF} == $src_finfo and $changed = 0;
				# If it's already right, don't do anything.
  }

  if($changed) {
    $dest_finfo->unlink;	# Get rid of anything that might already
				# be there.
    eval { $dest_finfo->symlink($src_finfo); }; # Make the link.
    if ($@) {			# Did something go wrong?
      main::print_error( 'Cannot link ', $src_finfo, ' to ', $dest_finfo, ":\n$@" );
      return 1;			# Indicate failure.
    }
  }

  # NOTE: This has to happen *after* the file exists (or else the build info
  # won't be saved), but *before* calling may_have_changed (which erases the
  # build info). Bad things could happen if it were possible for
  # update_build_infos to be called after NEEDS_BUILD_UPDATE is set, but
  # before now.
  update_build_infos();		# Update it now.  This way, the file is marked
				# as coming from a repository even if the
				# build command is aborted.  Next time around
				# we'll know that it came from a repository
				# and we can delete it appropriately.

  if ($changed) {
    my $build_info = $dest_finfo->{BUILD_INFO}; # Don't flush the build info.
                                # (If we lose the build info, then we don't
                                # clean up this file in
                                # cleaup_temporary_links.)
    $dest_finfo->may_have_changed if $changed; # Flush the stat array cache.
    $dest_finfo->{BUILD_INFO} = $build_info;
    $dest_finfo->{SIGNATURE} = $src_finfo->signature;
                                # Have to have the current signature or else
                                # the build info will get discarded anyway.
  }

  return 0;			# Indicate success.
}

#
# For aesthetic reasons, we need to get rid of all our temporary symbolic
# links when everything is done.
#
sub cleanup_temporary_links {
  return unless $FileInfo::made_temporary_link; # Don't bother if there weren't
				# any repository files used.

  return if $main::keep_repository_links;
				# Quit if we're supposed to keep all of the
				# soft links around.
  traverse sub {		# Look at every file we know anything about.
    foreach my $finfo (@_) {
      my $binfo = $finfo->{BUILD_INFO};	# Does this file have build info?
      next unless $binfo;	# If not, it wasn't linked from a repository.
      if ($binfo->{FROM_REPOSITORY}) {
				# Did we get the file from the repository?
	$finfo->unlink;		# Discard the file.
      }
    }
  };
}

=head2 name

  $string = $finfo->name;
  $string = $finfo->name($cwd);

Returns the name of the file.  With no extra argument, returns the absolute
filename; with the argument, returns the filename relative to that
directory.

=cut
sub name {
  ref($_[1]) eq 'FileInfo' and return &relative_filename;
  &absolute_filename;
}

=head2 set_additional_dependencies

  $finfo->set_additional_dependencies($dependency_string, $makefile, $makefile_line);

Indicates that the list of objects in $dependency_string are extra dependencies
of the file.  These dependencies are appended to the list of dependencies
from the rule to build the file.  $makefile and $makefile_line are used only
when we have to expand the list of dependencies.  We can't do this until we
actually need to make the file, because we might not be able to expand
wildcards or other things properly.

=cut

sub set_additional_dependencies {
  my ($finfo, $dependency_string, $makefile, $makefile_line) = @_;

  push(@{$finfo->{ADDITIONAL_DEPENDENCIES}},
       [ $dependency_string, $makefile, $makefile_line ]);
				# Store a copy of this information.
  $finfo->publish;		# For legacy makefiles, sometimes an idiom like
				# this is used:
				#   y.tab.c: y.tab.h
				#   y.tab.h: parse.y
				#	yacc -d parse.y
				# in order to indicate that the yacc command
				# has two targets.  We need to support this
				# by indicating that files with extra
				# dependencies are buildable, even if there
				# isn't an actual rule for them.
}

=head2 set_build_info_string

  $finfo->set_build_info_string($key, $value, $key, $value, ...);

Sets the build info string for the given key(s).  This can be read back in
later or on a subsequent build by build_info_string().

You should call update_build_infos() to flush the build information to disk,
or else it will never be stored.  It's a good idea to call
update_build_infos() fairly frequently, so that nothing is lost in the case of
a machine crash or someone killing your program.

=cut
sub set_build_info_string {
  my $finfo = shift @_;

  my $binfo = $finfo->{BUILD_INFO}; # Access the build information.
  unless ($binfo) {		# No known build information?
    $binfo = $finfo->{BUILD_INFO} = load_build_info_file($finfo) || {};
				# Load the old information from disk, if we
				# haven't already, so if there are fields we
				# aren't overriding, these are preserved.
  }

  my $update;
  while (@_) {
    my $key = shift @_;
    my $val = shift @_;
    die if $key eq "END";

    if(!defined($binfo->{$key}) || $binfo->{$key} ne $val) {
      $update = 1;
      $binfo->{$key} = $val;
    }
  }
  if($update) {
    $finfo->{NEEDS_BUILD_UPDATE} = 1;
                                # Remember that we haven't updated this
				# file yet.
    push @FileInfo::build_infos_to_update, $finfo;
  }
}

=head2 clear_build_info

  $finfo->clear_build_info;

Clears the build info strings for all keys.
The principal reason to do this would be that the file is about to be
regenerated.

=cut
sub clear_build_info {
  my $finfo = shift @_;

  $finfo->{BUILD_INFO} = {}; # Clear the build information.

  # Now remove the info file, if any. It's dangerous to leave this for
  # update_build_infos, because if the timestamp of a regenerated file was
  # the same and we stop before the build info is re-written, then we could
  # pick up stale info on the next makepp run.

  # This violates the DRY rule, but I'm not sure we can justify the overhead
  # of making it a method call:
  my $build_info_fname = $finfo->{".."}->absolute_filename . "/$FileInfo::build_info_subdir/" . $finfo->{NAME};
  CORE::unlink($build_info_fname); # Get rid of bogus file.
  # TBD: What to do if it's still there (e.g. no directory write privilege)?
  delete $finfo->{NEEDS_BUILD_UPDATE}; # No need to update at the moment.
}

=head2 set_rule

  $finfo->set_rule($rule);

Sets a rule for building the specified file.  If there is already a rule,
which rule overrides is determined by the following procedure:

=over 4

=item 1.

A rule that recursively invokes make never overrides any other rule.
This is a hack necessary to deal with some legacy makefiles which have
rules for targets that actually invoke the proper rule in some other
makefile, something which is no longer necessary with makepp.

=item 2.

If either rule is an explicit rule, and not a pattern rule or a backward
inference rule, then the explicit rule is used.	 If both rules are
explicit rules, then this is an error.

Note that a pattern rule which is specified like this:

  %.o: %.c : foreach abc.c def.c ghi.c

where no wildcards are involved is treated as an explicit rule for
abc.o, def.o, and ghi.o.

=item 3.

A pattern rule overrides a backward inference rule.  (This should never
happen, since backward inference rules should only be generated if no pattern
rule exists.)

=item 4.

A pattern rule from a "nearer" makefile overrides one from a "farther"
makefile.  Nearness is determined by the length of the relative file
name of the target compared to the makefile's cwd.

=item 5.

A pattern rule seen later overrides one seen earlier.  Thus more specific
pattern rules should be placed after the more general pattern rules.

=item 6.

A builtin rule is always overridden by any other kind of rule, and never
overrides anything.

=back

=cut
sub set_rule {
  my ($finfo, $rule) = @_; # Name the arguments.

  if (!defined($rule)) {	# Are we simply discarding the rule now to
				# save memory?	(There's no point in keeping
				# the rule around after we've built the thing.)
    $finfo->{RULE} = undef if exists $finfo->{RULE};
				# Just keep a marker around that there used
				# to be a rule.
    return;
  }

  my $rule_is_default = ($rule->source =~ /\bmakepp_builtin_rules\.mk:/);

  $finfo->{IS_PHONY} &&		# If we know this is a phony target, don't
    $rule_is_default and	# ever let a default rule attempt to build it.
      return;

  my $oldrule = $finfo->{RULE};	# Is there a previous rule?

  if ($oldrule && $oldrule->{LOAD_IDX} < $oldrule->{MAKEFILE}{LOAD_IDX}) {
    $oldrule = undef;		# If the old rule is from a previous load
				# of a makefile, discard it without comment.
    delete $finfo->{BUILD_HANDLE}; # Avoid the warning message below.  Also,
				# if the rule has genuinely changed, we may
				# need to rebuild.
  }

  if ($oldrule) {
    my $oldrule_is_default = ($oldrule->source =~ /\bmakepp_builtin_rules\.mk:/);
    if ($rule_is_default) {	# Never let a builtin rule override a rule
      return;			# in the makefile.
    }
    elsif (!$oldrule_is_default) { # If the old rule isn't a default rule:
      $main::log_level and
	main::print_log("Alternate rules (", $rule->source, " and ",
			$oldrule->source, ") for target ",
			$finfo->name);

      my $new_rule_recursive = ($rule->{COMMAND_STRING} || '') =~ /\$[\(\{]MAKE[\)\}]/;
      my $old_rule_recursive = ($oldrule->{COMMAND_STRING} || '') =~ /\$[\(\{]MAKE[\)\}]/;
				# Get whether the rules are recursive.

      if ($new_rule_recursive && !$old_rule_recursive) {
				# This rule does not override anything if
				# it invokes a recursive make.
	$main::log_level and
	  main::print_log(" Rule ", $rule->source,
			  " ignored because it invokes \$(MAKE)");
	return;
      }

      if ($old_rule_recursive && !$new_rule_recursive) {
	$main::log_level and
	  main::print_log(" Rule ", $oldrule->source,
			  " discarded because it invokes \$(MAKE)");

	delete $finfo->{BUILD_HANDLE};
				# Don't give a warning message about a rule
				# which was replaced, because it's ok in this
				# case to use a different rule.
      }
      else {
	if (!$oldrule->{PATTERN_LEVEL}) {	# Old rule not a pattern rule?
	  if ($rule->{PATTERN_LEVEL}) { # New rule is?
	    $main::log_level and
	      main::print_log(" Rule ", $rule->source, " ignored because it is a pattern rule");
	    return;
	  }
	  else {
	    main::print_warning( 'conflicting rules `', $rule->source,
				 "' and `", $oldrule->source, "' for target ",
				 $finfo );
	  }

	}			# End if old rule was an explicit rule.
	else {
	  #
	  # Apparently both are pattern rules.	Figure out which one should override.
	  #
	  if ($rule->{MAKEFILE} != $oldrule->{MAKEFILE}) { # Skip comparing
				# the cwds if they are from the same makefile.
	    if (length($rule->build_cwd->relative_filename($finfo->{".."})) <
		length($oldrule->build_cwd->relative_filename($finfo->{".."}))) {
	      $main::log_level and
		main::print_log(" Rule ", $rule->source,
				" chosen because it is from a nearer makefile");
	    } else {
	      $main::log_level and
		main::print_log(" Rule ", $oldrule->source,
				" kept because it is from a nearer makefile");
	      return;
	    }
	  } else {		# If they're from the same makefile, use the
				# one that has a shorter chain of inference.
	    if (($rule->{PATTERN_LEVEL} || 0) < $oldrule->{PATTERN_LEVEL}) {
	      $main::log_level and
		main::print_log(" Rule ", $rule->source,
				" chosen because it has a shorter chain of inference");
	    } elsif (($rule->{PATTERN_LEVEL} || 0) > $oldrule->{PATTERN_LEVEL}) {
	      $main::log_level and
		main::print_log(" Rule ", $oldrule->source,
				" chosen because it has a shorter chain of inference");
	      return;
	    } else {
	      if ($rule->source eq $oldrule->source) {
		main::print_warning( 'rule `', $rule->source, "' produces ", $finfo,
				     " in two different ways" );
	      }
	    }
	  }
	}
      }
    }
  }

#
# If we get here, we have decided that the new rule (in $rule) should override
# the old one (if there is one).
#
#  $main::log_level and
#    main::print_log(0, $rule->source, " applies to target ", $finfo->name);

  $finfo->{RULE} = $rule;	# Store the new rule.
  $finfo->{PATTERN_LEVEL} = $rule->{PATTERN_LEVEL} if $rule->{PATTERN_LEVEL};
				# Remember the pattern level, so we can prevent
				# infinite loops on patterns.  This must be
                                # set before calling publish(), or we'll get
                                # infinite recursion.
  $rule->{LOAD_IDX} = $rule->{MAKEFILE}{LOAD_IDX};
				# Remember which makefile load it came from.

  if (exists $finfo->{BUILD_HANDLE} &&
      $finfo->{RULE} &&
      ref($finfo->{RULE}) ne 'DefaultRule' &&
      $rule->source !~ /\bmakepp_builtin_rules\.mk:/) {
    main::print_warning( 'I became aware of the rule `', $rule->source,
      "' for target ", $finfo, ' after I had already tried to build it using the default rules' );
  }
  publish($finfo);		# Now we can build this file; we might not have
				# been able to before.
}

=head2 signature

   $str = $fileinfo->signature;

Returns the signature of this file.  The signature is usually the file time,
but users can change it to be anything they want.

Returns undef if the file doesn't exist.

=cut

sub signature {
  my $finfo = $_[0];
  return $finfo->{SIGNATURE} if $finfo->{SIGNATURE};
				# Return the cached value, if there is one.
  my $sigfunc = $finfo->{SIGNATURE_FUNC} || \&default_signature_func;

  my $sig = &$sigfunc($finfo);

  if ($sig) {
    publish($finfo);		# If we have a signature, activate any wildcard
				# routines eagerly waiting for the appearance
				# of this file.
    $finfo->{SIGNATURE} = $sig;	# Cache the signature.
  }
  return $sig;
}

=head2 update_build_infos

  FileInfo::update_build_infos();

Flushes our cache of build information to disk.	 You should call this fairly
frequently, or else if the machine crashes or some other bad thing happens,
some build information may be lost.

=cut
sub update_build_infos {

  local *BUILD_INFO;

  foreach my $finfo (@FileInfo::build_infos_to_update) {
    next unless $finfo->{NEEDS_BUILD_UPDATE};
				# Skip if we already updated it.  If two
				# build info strings for the same file are
				# changed, it can get on the list twice.
#    print "Updating build info for ", $finfo->name, "\n";
    delete $finfo->{NEEDS_BUILD_UPDATE}; # Do not update it again.
    my $build_info_subdir = $finfo->{".."}->absolute_filename_nolink .
      "/$FileInfo::build_info_subdir";

    my $build_info_subdir_info = file_info($FileInfo::build_info_subdir, $finfo->{".."});
    $build_info_subdir_info->mkdir; # Make sure the build info subdir exists.

    my $build_info_fname = "$build_info_subdir/$finfo->{NAME}";
				# Form the name of the build info file.

    my $build_info = $finfo->{BUILD_INFO}; # Access the hash.
    $build_info->{SIGNATURE} ||= $finfo->signature;
				# Make sure we have a valid signature.	Use
				# ||= instead of just = because when we're
				# called to write the build info for a file
				# from a repository, the build info is created
				# before the link to avoid the race condition
				# where a soft link is created and we are
				# interrupted before marking it as from a
				# repository.
    $build_info->{SIGNATURE} or next;
				# If the file has been deleted, don't bother
				# writing the build info stuff.
    open(BUILD_INFO, "> $build_info_fname") || next;
				# If we can't write it, don't bother saving it.
    my ($key, $val);

    while (($key, $val) = each %$build_info) {
      $key =~ s/([=\n\\])/sprintf '\%03o', ord $1/eg;
      $val =~ s/([\n\\])/sprintf '\%03o', ord $1/eg;
				# Protect any special characters.
				# (This does not modify the value inside the
				# BUILD_INFO hash.)
      print BUILD_INFO "$key=$val\n";
    }
    # This provides proof that the writing of the build info file was not
    # interrupted. It's compatible if you back-rev to makepp 1.19.
    print BUILD_INFO "END=\n";
    close BUILD_INFO;
  }
  @FileInfo::build_infos_to_update = ();
				# Clean out the list of files to update.
}

=head2 was_built_by_makepp

   $built = $fileinfo->was_built_by_makepp;

Returns TRUE iff the file was put there by makepp and not since modified.

=cut
sub was_built_by_makepp {
  my $finfo = $_[0];
  return defined($finfo->build_info_string("BUILD_SIGNATURE")) ||
    defined($finfo->build_info_string("FROM_REPOSITORY"));
}

=head2 is_stale

   $stale = $fileinfo->is_stale;

Returns TRUE iff the file was put there by makepp and not since modified, but
now there is no rule for it, or it is not from a repository and the only
rule for it is to get it from a repository.

=cut
sub is_stale {
  my $finfo = $_[0];
  return !exists($finfo->{RULE}) && !$finfo->{ADDITIONAL_DEPENDENCIES} &&
    !$finfo->dont_build && (
      defined($finfo->build_info_string("BUILD_SIGNATURE")) ||
      (
        defined($finfo->build_info_string("FROM_REPOSITORY")) &&
        !($finfo->{ALTERNATE_VERSIONS} && @{$finfo->{ALTERNATE_VERSIONS}})
      )
    );
}

=head2 assume_unchanged

Returns TRUE iff the file or directory is assumed to be unchanged.
A file or directory is assumed to be unchanged if any of its ancestor
directories are assumed unchanged.

=cut
sub assume_unchanged {
  my $finfo = $_[0];
  return $finfo->{ASSUME_UNCHANGED} if exists($finfo->{ASSUME_UNCHANGED});
  return undef unless $main::assume_unchanged_dir_flag;
  return undef if $finfo == $FileInfo::root;
  return $finfo->{ASSUME_UNCHANGED} = ($finfo->{".."}->assume_unchanged);
}

=head2 dont_build

Returns TRUE iff the file or directory is marked for don't build.
A file or directory is treated as marked for don't build if any of its ancestor
directories are so marked.

=cut
sub dont_build {
  my $finfo = $_[0];
  return $finfo->{DONT_BUILD} if exists($finfo->{DONT_BUILD});
  return undef unless $main::dont_build_dir_flag;
  return undef if $finfo == $FileInfo::root;
  return $finfo->{DONT_BUILD} = ($finfo->{".."}->dont_build);
}

###############################################################################
#
# Internal subroutines (don't call these):
#

#
# The default signature function.  This returns the file modification time.
#
# Arguments:
# a) The finfo struct.
#
sub default_signature_func {
  my ($finfo) = $_[0];
  $finfo->is_dir and return 1;	# If this is a directory, the modification time
				# is meaningless (it's inconsistent across
				# file systems, and it may change depending
				# on whether the contents of the directory
				# has changed), so just return a non-zero
				# constant.
  return $finfo->file_mtime;
}

#
# Load a build info file, if it matches the signature on the actual file.
# Returns undef if this build info file didn't exist or wasn't valid.
# Arguments:
# a) The FileInfo struct for the file.
#
sub load_build_info_file {
  my $finfo = $_[0];

  my %build_info;

  my $build_info_fname = $finfo->{".."}->absolute_filename . "/$FileInfo::build_info_subdir/" . $finfo->{NAME};
				# Form the name of the info file.

  local *BUILD_INFO;	# Make a local file handle.
  return undef unless open(BUILD_INFO, $build_info_fname);
				# Access the file.
  my $sig = $finfo->signature || ''; # Calculate the signature for the file, so
				# we know whether the build info has changed.
  my $line;
  my ($end, $corrupt);
  while (defined($line = <BUILD_INFO>)) {	# Read another line.
    if($end) {
      $corrupt = 1;
      last;
    }
    $line =~ s/\r//;		# Strip out the silly windows-format lines.
    if ($line =~ /(.*?)=(.*)/) { # Check the format.
      my $key = $1;
      my $val = $2;
      if($key eq "END") {
        $end = 1;
        $corrupt = 1 unless $val eq "";
        next;
      }
      $val =~ s/\\([0-7]{1,3})/chr(oct($1))/eg;
      $key =~ s/\\([0-7]{1,3})/chr(oct($1))/eg;
				# Convert special characters.
      $build_info{$key} = $val;
    }
    else {
      $corrupt = 1;
      last;
    }
  }
  close BUILD_INFO;

  # TBD: *After* the next release, remove the "|| 1". It's there now so that
  # it doesn't complain about the files left behind by makepp 1.19. Also,
  # remove the "|| !$end" in the following "if" statement.
  $corrupt = 1 unless $end || 1;

  warn $build_info_fname . ": build info file corrupted\n" if $corrupt;

  # If we linked the file in from a repository, but it was since modified in
  # the repository, then we need to remove the link to the repository now,
  # because otherwise we won't remove the link before the target gets built.
  my $sig_mismatch = (($build_info{SIGNATURE} || '') ne $sig);
  if(
    ($sig_mismatch ||
      !$finfo->{ALTERNATE_VERSIONS} || !@{$finfo->{ALTERNATE_VERSIONS}}
    ) && exists($build_info{FROM_REPOSITORY})
  ) {
    if(
    $finfo->dereference ==
      file_info($build_info{FROM_REPOSITORY}, $finfo->{".."})
    ) {
      $finfo->unlink; # Nuke the outdated repository symlink
    }
    else {
      $sig_mismatch = 1; # The symlink was modified outside of makepp
    }
  }

  if ($corrupt || !$end || $sig_mismatch) {
				# Exists but has the wrong signature?
    main::print_log(
      "Discarding out-of-date or corrupt build info file $build_info_fname"
    );
    CORE::unlink($build_info_fname); # Get rid of bogus file.
    # NOTE: We assume that if we failed to unlink $finfo, then we'll fail to
    # unlink $build_info_fname as well, so that the FROM_REPOSITORY turd will
    # remain behind, which is what we want. Furthermore, because we remember
    # that we tried to unlink $finfo, it should appear to makepp that it
    # no longer exists, which is also what we want.
    return undef;
  }

  return \%build_info;
}

1;
