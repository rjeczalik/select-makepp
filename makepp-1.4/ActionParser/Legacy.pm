=head1 NAME

ActionParser::Legacy - Makepp scanner class for legacy interface

=head1 SYNOPSIS

	use ActionParser::Legacy;
	my $scanner=ActionParser::Legacy->new(\&scanner_c_compilation);

=head1 DESCRIPTION

C<ActionParser::Legacy> is an adapter class of type C<ActionParser> that can
talk to the legacy scanner interface.
Its use is deprecated.

=cut

use strict;
package ActionParser::Legacy;
use ActionParser;
use vars qw/@ISA/;
@ISA=qw/ActionParser/;


=head1 METHODS

=head2 new

	my $rp = ActionParser::Legacy->new($coderef);

Returns a new ActionParser object that always uses $coderef as its command
parser.

=cut

sub new {
  my $self=shift;
  my $class=ref($self)||$self;
  my ($coderef)=@_;
  bless { COMMAND_SCANNER => $coderef }, $class;
}

=head2 find_command_parser

	my $cp = $rp->find_command_parser($command, $rule, $dir);

Call the predefined coderef with $command, $rule and $dir, and return 0.

=cut

sub find_command_parser {
  my $self=shift;
  my ($command, $rule, $dir)=@_;
  my $scanner=$self->{COMMAND_SCANNER};
  $rule->mark_scaninfo_uncacheable;
  &$scanner($command, $rule, $dir);
  return 0;
}

1;

