=head1 NAME

ActionParser::Specific - Makepp scanner class for a specified command scanner

=head1 SYNOPSIS

	use ActionParser::Specific;
	my $scanner=ActionParser::Specific->new("CommandParser::Vcs");

=head1 DESCRIPTION

C<ActionParser::Specific> is a class of type C<ActionParser> that always
chooses the specified command parser.

=cut

use strict;
package ActionParser::Specific;
use ActionParser;
use vars qw/@ISA/;
@ISA=qw/ActionParser/;


=head1 METHODS

=head2 new

	my $rp = ActionParser::Specific->new($command_parser_class_name);

Returns a new ActionParser object that always uses a default-constructed
object of class $command_parser_class_name as its command parser.

=cut

sub new {
  my $self=shift;
  my $class=ref($self)||$self;
  my ($cp_class)=@_;
  $cp_class = "CommandParser::$cp_class" unless $cp_class =~ /^CommandParser::/;
  eval "require $cp_class";
  unless($cp_class->can("new")) {
    warn $@ if $@;
    die "Unable to find `new' method via class $cp_class\n";
  }
  bless { COMMAND_PARSER_CLASS => $cp_class }, $class;
}

=head2 find_command_parser

	my $cp = $rp->find_command_parser($command, $rule, $dir);

Construct the predefined CommandParser class object with $rule and $dir.

=cut

sub find_command_parser {
  my $self=shift;
  shift; # discard command text
  my $cp_class=$self->{COMMAND_PARSER_CLASS};
  return $cp_class->new(@_);
}

1;

