# $Id: shared_object.pm,v 1.1 2004/04/08 00:12:51 grholt Exp $
use strict;
package Signature::shared_object;

use Signature::c_compilation_md5;

use vars qw/@ISA $shared_object/;

@ISA = qw(Signature::c_compilation_md5);

=head1 NAME

Signature::shared_object -- a signature class that ignores changes to shared objects

=head1 DESCRIPTION

Normally, if you build a shared object, you do not have to relink anything
that depends on that shared object.  This class sets the signature of a shared
object to the same value, regardless of its contents.

For files which are not shared objects, the signature is the same as for C<c_compilation_md5>.

=cut

$shared_object = bless {};	# Make the singleton object.

# Things that can be overridden by a derived class:
sub build_info_key {
	return "SHARED_OBJECT";
}

#
# The only subroutine we need to override is the signature method; we use
# exact matching of MD5 signatures.
#
sub signature {
  my ($self, $finfo, $for_build_check) = @_;	# Name the arguments.
  $finfo->{NAME} =~ /\.s[oa]$/ and
    return $finfo->file_exists ? "exists" : "";

  &Signature::c_compilation_md5::signature;
}

#
# This function calculates the MD5 sum of the interface of a shared library.
# Normally, when a shared library changes, one does not need to relink;
# it's only if the externally visible variables change that a relink might
# be necessary.  This function computes an MD5 checksum on the externally
# visible symbols.
#
# This function is not currently used because it seems to trigger too often.
# This will probably only work with GNU binutils, anyway.
#
# Arguments:
# a) The singleton object.
# b) The file info for the shared library.
#
sub signature_shared_lib {
  my ($self, $finfo) = @_;

  my $cksum = $finfo->build_info_string("SO_INTERFACE");
  return $cksum if $cksum;      # Don't bother rescanning if the
                                # file hasn't changed.

  local *NM;
  open(NM, "nm -D " . $finfo->absolute_filename . " 2>/dev/null |") or
    return undef;
  local $_;
  my %undef_symbols;
  my %def_symbols;
  my $n_symbols = 0;
  while (defined($_ = <NM>)) {  # Keep reading lines.
    
    if (/ T (\S.+)$/) { ++$def_symbols{$1}; ++$n_symbols; }
    elsif (/ U (\S.+)$/) { ++$undef_symbols{$1}; }
  }
  return undef unless $n_symbols; # If that didn't work, then fall back to the
                                # regular signature.

  my $ctx = Digest::MD5->new;	# Make a context.
  foreach (sort keys %def_symbols) { $ctx->add("$_\01"); }
  foreach (sort keys %undef_symbols) { $ctx->add("$_\02"); }
  close NM;
  $cksum = $ctx->hexdigest;

  $finfo->set_build_info_string("SO_INTERFACE", $cksum);
  return $cksum;
}

1;
