# $Id: target_newer.pm,v 1.7 2004/04/13 21:02:43 topnerd Exp $
package Signature::target_newer;

use Signature;

@ISA = qw(Signature);

=head1 NAME

Signature::target_newer -- a signature class that uses the traditional Make algorithm

=head1 USAGE

This is the default signature class for a few special cases, e.g., for rules
that build Makefile or Makefile.in.  Otherwise, if you want to use it, you
must specify it on the command line or explicitly in rules:

   target : dependencies
	  : signature exact_match
	$(commands_to_build)

=head1 DESCRIPTION

This class forces a recompilation if the target is older than any of its
dependencies.  It also does not require the command to be the same as on the
last build, nor does it it make sure that the architecture is the same.	 This
is generally not a reliable way to build things, but it is occasionally useful
for special purpose things where a target may be modified by some commands not
executed under make's control.	For example, if you want your makefile to pull
the latest version out of an RCS archive, but only if the RCS archive is more
recent, you could have a rule like this:

   %: %,v
    : signature target_newer
	co $(FIRST_DEPENDENCY)

If you did not add the "C<:signature target_newer>", the rule would not work as
expected.  If you checked the file out of the RCS archive, then modified it,
B<makepp>'s default rule would notice that the file's signature did not match
the signature from the last build, so it would check it out for you again.

=cut

$target_newer = bless {};	# Make the singleton object.

sub signature {
  return $_[1]->file_mtime;
}

sub build_check {
  my ($self, $tinfo, $sorted_dependencies) = @_;
			        # Ignore the arguments we don't need.
  my $tsig = $tinfo->file_mtime; # Get the timestamp on the target.
  unless ($tsig) {		# If there's no target, then we need to
				# rebuild.
    $main::log_level and
      main::print_log("Rebuild because target doesn't exist");
    return 1;
  }

  foreach $dep (@$sorted_dependencies) {
    next if $dep->assume_unchanged; # Skip if this file is one of the ones we
                                # don't want to check at all.
    if ($dep->{ASSUME_CHANGED}) {   # Marked by --assume-new option?
      main::print_log("Rebuild ", $tinfo, " because ", $dep, " is marked new");
      return 1;
    }
    my $mtime = $dep->{BUILT} ? undef : $dep->file_mtime;
    if (!defined($mtime) || $mtime > $tsig) {
      main::print_log("Rebuild ", $tinfo, " because ", $dep, " is newer");
      return 1;
    }
  }

  main::print_log("Rebuild of ", $tinfo->name, " not needed");
  return undef;			# No rebuild necessary.
}

1;
