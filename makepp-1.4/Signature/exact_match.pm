# $Id: exact_match.pm,v 1.10 2004/04/13 21:02:43 topnerd Exp $
package Signature::exact_match;

use strict;

use Signature;

@Signature::exact_match::ISA = qw(Signature);

=head1 NAME

Signature::exact_match -- a signature class that requires that dependencies have exactly the same signature they did on the last build

=head1 USAGE

This is the default signature class.  You don't have to do anything to use it
unless you override it from the command line.  You can specify it explicitly
in the Makefile using

   target : dependencies
	  : signature exact_match
	$(commands_to_build)


=head1 DESCRIPTION

This class forces a recompilation if any of the following are different from
the last time this particular rule was executed:

=over 4

=item *

The build command or the directory from which it is executed.

=item *

The architecture this is running on (e.g., i386, sparc, alpha, etc.).

=item *

The signatures of each dependency.

=back

=cut

my $last_compilation_warning_x86 = 0;

$Signature::exact_match::exact_match = bless {}; # Make the singleton object.

# We can't rely on the timestamp to be different if the file was built, but
# build_check should always behave as if it were.
sub signature_for_build_check {
	return $_[1]->{BUILT} ? undef : $_[1]->signature;
}

#
# See documentation in Signature.pm for details of the arguments and
# intended side effects.
#
sub build_check {
  my (
    $self, $tinfo, $sorted_dependencies, $command_string, $build_cwd, $outdated
  ) = @_;

  die if $outdated && @$outdated; # @$outdated must start out empty.
#
# Do some quick checks that depend on the targets alone:
# Note that we don't have to check the signature, because if it doesn't match,
# the build info file will be invalid anyway.  See FileInfo::load_build_info.
#
  if ($command_string ne ($tinfo->build_info_string("COMMAND") || '')) {
    if ($main::log_level) {
      my ($last_cmd, $this_cmd);
      ($last_cmd = $tinfo->build_info_string("COMMAND") || '') =~ s/\n/\\n/g;
      ($this_cmd = $command_string) =~ s/\n/\\n/g;
				# Make the strings more printable.
      main::print_log("Rebuild ", $tinfo->name, " because last build command (\"$last_cmd\") differs from current command (\"$this_cmd\")");
    }

    return 1;
  }

#
# We used to make sure the build cwd is the same.  We do not check this any
# more, because there are evidently a lot of programs (the linux kernel is
# a horrifying example of this) where the same file can be built from multiple
# makefiles, with different CWDs.  Usually this is for files whose actions
# are just "echo" or "touch", things which do not care about the current
# directory.
#
# Generally speaking, if the build cwd changes, so does the command, so
# it is probably ok not to bother checking the build cwd.
#
#    if ($build_cwd ne ($tinfo->build_info_string("CWD") || '')) {
#      $main::log_level and
#	 main::print_log("Rebuild ", $tinfo->name, " because build cwd changed\n");
#      return 1;
#    }

  if ($main::architecture ne ($tinfo->build_info_string("ARCH") || '')) {
#
# The pentium architectures are all more or less equivalent, but have different
# architecture flags.  Give a warning (so at least the user is not surprised
# about recompilation).
#
    if ($main::architecture =~ /^i[34567]86-linux/ &&
	$tinfo->build_info_string("ARCH") =~ /^i[34567]86-linux/) {
      unless ($last_compilation_warning_x86++) {
	$main::warn_level and
	  main::print_warning("last compilation was on the ",
			    $tinfo->build_info_string("ARCH"),
			    " architecture,
  and this is on $main::architecture.
  These are technically different and force a recompilation of everything,
  but this may not be what you want.  The difference is most likely caused
  by running a different copy of perl.");
      }
    }
    $main::log_level and
      main::print_log("Rebuild ", $tinfo->name, " because last build was on ",
		      $tinfo->build_info_string("ARCH") || '',
		      " and this is on ", $main::architecture);
    return 1;
  }

#
# If we get here, we have to scan through all of the dependencies
# to see if any of them has changed.
# First sort the dependencies:
#
  my @old_dep_list = map { FileInfo::file_info($_, $build_cwd) }
    split(/\01/, $tinfo->build_info_string("SORTED_DEPS"));
  my @old_dep_sigs = split(/\01/, $tinfo->build_info_string("DEP_SIGS"));

  if (@old_dep_list != @$sorted_dependencies) { # Different # of dependencies?
    report_changed_dependencies(
      \@old_dep_list, $sorted_dependencies, $tinfo, $build_cwd
    );
    return 1;
  }

  for (my $depidx = 0; $depidx < @$sorted_dependencies; ++$depidx) {
				# Scan through the dependencies one at a time:
    my $dep = $sorted_dependencies->[$depidx]; # Access the fileinfo.
    next if $dep->assume_unchanged; # Skip if this file is one of the ones we
				# don't want to check at all.
    if ($dep->{ASSUME_CHANGED}) {   # Marked by --assume-new option?
      main::print_log("Rebuild ", $tinfo, " because ", $dep, " is marked new");
      $outdated ? push(@$outdated, $dep) : return 1;
    }
    if ($old_dep_list[$depidx] != $dep) {
      report_changed_dependencies(
        \@old_dep_list, $sorted_dependencies, $tinfo, $build_cwd
      );
      @$outdated=() if $outdated;
      return 1;
    }

    my $sig = $self->signature_for_build_check($dep);
    if (!defined($sig) || $sig ne $old_dep_sigs[$depidx]) {
      main::print_log("Rebuild ", $tinfo, " because ", $dep, " changed");
      $outdated ? push(@$outdated, $dep) : return 1;
    }
  }
  return 1 if $outdated && @$outdated;

  return undef;		# No build necessary.
}

#
# This subroutine is used only for printing to the log file.  It analyzes
# two lists of files and figures out exactly what's different between them.
#
sub report_changed_dependencies {
  $main::log_level or return;	# Don't do anything if not logging.

  my ($old_deps, $new_deps, $tinfo, $build_cwd) = @_;

  main::print_log("Rebuild ", $tinfo, " because dependency list is different");
  my %old_deps = ();
  my @not_in_old_deps;
  foreach (@$old_deps) {
    $old_deps{$_->name($build_cwd)} = 1;
  }				# Get array of all old dependencies.
  foreach (@$new_deps) {
    my $fname = $_->name($build_cwd);
    if (exists($old_deps{$fname})) {
      delete $old_deps{$fname}; # Perform a set difference.
    } else {
      push @not_in_old_deps, $fname;   # Record any files which are new.
    }
  }
  if (%old_deps) {		# Anything left in old list?
    main::print_log("Files in old dependency list but not in new:");
    foreach (sort keys %old_deps) {
      main::print_log("	 $_");
    }
  }
  if (@not_in_old_deps) {
    main::print_log("Files in new dependency list but not in old:");
    foreach (@not_in_old_deps) {
      main::print_log("	 $_");
    }
  }
}

1;
