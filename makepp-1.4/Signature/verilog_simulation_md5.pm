# $Id: verilog_simulation_md5.pm,v 1.1 2003/12/25 01:47:00 topnerd Exp $
use strict;
package Signature::verilog_simulation_md5;

use Signature::c_compilation_md5;

use vars qw/@ISA $verilog_simulation_md5/;

@ISA = qw(Signature::c_compilation_md5);

=head1 NAME

Signature::verilog_simulation_md5 -- a signature class that ignores changes to whitespace and comments

=head1 DESCRIPTION

Very similar to Signature::c_compilation_md5, except that it recognizes
different filenames.

=cut

$verilog_simulation_md5 = bless {};	# Make the singleton object.

sub recognizes_file {
  my $finfo = $_[1];
  return $finfo->{NAME} =~ /\.v$/;
}

1;
