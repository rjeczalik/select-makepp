# $Id: Rule.pm,v 1.21.2.1 2004/05/12 16:50:28 pfeiffer Exp $
use strict qw(vars subs);

package Rule;

use MakeEvent qw(when_done wait_for);
use FileInfo;
use FileInfo_makepp;
use TextSubs;
use Signature::exact_match;
use ActionParser;

=head1	NAME

Rule -- Stores information about a build rule

=head1 USAGE

  my $rule = new Rule "targets", "dependencies",
    "command"[, $makefile, $makefile_line];

=head1 DESCRIPTION

A rule object contains all the information necessary to make a specific file.

$makefile is a pointer to the structure returned by load_makefile.
$makefile_line is an ASCII string that is used if an error message occurs
while expanding variables in this rule.

This does not actually place the rule into the FileInfo hierarchy.

=cut
sub new {
  my ($class, $targets, $dependencies, $command, $makefile, $source_line) = @_;
				# Name the arguments.

  return bless { TARGET_STRING => $targets,
		 DEPENDENCY_STRING => $dependencies,
		 COMMAND_STRING => $command,
		 MAKEFILE => $makefile,
		 LOAD_IDX => $makefile->{LOAD_IDX},
		 RULE_SOURCE => $source_line }, $class;
				# Make the rule object.
}

#
# Return the directory that this rule should be executed in.
#
sub build_cwd {
  return $_[0]->{MAKEFILE}{CWD};
}

#
# Return the makefile that made this rule:
#
sub makefile {
  return $_[0]->{MAKEFILE};
}

#
# This subroutine is called to find all the targets and dependencies of the
# rule.	 It does so by repeatedly expanding the dependency and target string,
# and repeatedly scanning the command, until there are no further changes.
#
# Usage:
#   ($all_targets, $all_dependencies, $action_string) =
#     $rule->find_all_targets_dependencies($oinfo);
#
# Where all_targets is a reference to a list of target object info structs,
# all_dependencies is a reference to a list of dependency object info structs,
# and $action_string is the final build command after expanding all make
# variables. $oinfo is the requested target, which is used only to determine
# where to look for cached scan information.
#
sub find_all_targets_dependencies {
  my ($self, $oinfo) = @_;

  my $build_cwd = $self->build_cwd; # Access the default directory.

  my %all_targets;		# A hash of all targets, so we can tell quickly
				# whether we already know about a given target.
  local $Makesubs::rule = $self; # Set this up so that subroutines can find the
				# rule that is currently being expanded.

  local $self->{ALL_TARGETS} = \%all_targets;
				# Store this so it can be found easily but
				# also goes away automatically.

  my %all_dependencies;		# A hash of all dependencies, so we can tell
				# whether we already know about a given
				# dependency.
  local $self->{ALL_DEPENDENCIES} = \%all_dependencies;

  my @explicit_dependencies;	# The ones that were listed in the dependency
				# list or explicitly somewhere else in the
				# makefile.
  local $self->{EXPLICIT_DEPENDENCIES} = \@explicit_dependencies;

  my @explicit_targets;		# The ones that were listed and not inferred
				# from the command.

  local $self->{EXPLICIT_TARGETS} = \@explicit_targets;

  my @extra_dependencies;
  local $self->{EXTRA_DEPENDENCIES} = \@extra_dependencies;

  my $makefile = $self->makefile;

#
# Get the full list of explicit targets:
#
  my $target_string =
    $makefile->expand_text($self->{TARGET_STRING}, $self->{RULE_SOURCE});


  foreach (split_on_whitespace($target_string)) {
    my $tinfo = main::find_makepp_info(unquote($_), $build_cwd);
    push @explicit_targets, $tinfo;
    $self->add_target($tinfo);
  }

#
# Get the full list of explicit dependencies:
#
  my $dependency_string =
    $makefile->expand_text($self->{DEPENDENCY_STRING}, $self->{RULE_SOURCE});
				# Get the list of files.

  foreach (split_on_whitespace($dependency_string)) {
    my $fname = unquote($_);	# Handle quoting.
    if (/[\[\*\?]/) {		# Is it a wildcard?
      push @explicit_dependencies, Glob::zglob_fileinfo($fname, $build_cwd);
    } else {
      push @explicit_dependencies, main::find_makepp_info($fname, $build_cwd);
    }
  }

  push @explicit_dependencies, @extra_dependencies;
				# Extra dependencies go at the end of the
				# list.

  foreach (@explicit_dependencies) {
    $self->add_dependency($_);	# Make sure we know about each dependency.
  }

#
# Now expand the command string.  This must be done last because it
# can depend on both the explicit targets and the explicit dependencies.
#
  my $perl = 0;	      # split-pattern always 2nd in list, even if 1st is empty
  my $command_string = join '', map {
    if (!($perl = !$perl))	# so this one is perl, and next won't be :-)
    {
      $_;
    }
    elsif (@explicit_targets && $explicit_targets[0]->{TARGET_SPECIFIC_VARS})
    {
      local $Makefile::target_specific = $explicit_targets[0]->{TARGET_SPECIFIC_VARS};
				# Temporarily set up target-specific variables,
				# if there actually are any.
      local $Makefile::target_specific_reexpand = $explicit_targets[0]->{TARGET_SPECIFIC_REEXPAND};
				# Remember the difference between = and :=
				# variables for target-specific vars too.
      $makefile->expand_text($_, $self->{RULE_SOURCE});
				# Get the text of the command.
    }				# Turn off the target-specific variables.
    else {
      $makefile->expand_text($_, $self->{RULE_SOURCE});
				# Get the text of the command.
    }
  } split /^((?:[\@-]|noecho\s+|ignore_error\s+)*perl\s*\{(?s:\{.*?\}\})?.*\n?)/m, $self->{COMMAND_STRING};

  $command_string =~ s/^\s+//;	# Strip out leading and trailing whitespace
  $command_string =~ s/\s+$//;	# so we don't trigger unnecessary rebuilds
				# quite as often.

  # Try to get the scanner results from the build info, and failing that
  # scan from scratch. (Well, not completely from scratch, because the cached
  # lists of include directives can still be used if they're up-to-date.)
  if(my $msg=$self->load_scaninfo(
      $oinfo, $command_string, \@explicit_dependencies
    )
  ) {
    main::print_log(" Scanning rule for $oinfo->{NAME} because $msg\n");
    $self->parser()->parse_rule($command_string, $self) or
      $self->{SCAN_FAILED} = 1;
				# Look for any additional dependencies (or
				# targets) that we didn't know about.
  }
  else {
    main::print_log(" Using cached scanner info for $oinfo->{NAME}\n");
  }

#
# For some reason, the #@!$@#% linux kernel makefiles have a file that
# depends on itself.  This looks like a simple mistake, but I want this
# to work properly on the linux kernel, so we explicitly remove dependencies
# that are equal to the target.
#
  foreach (values %all_targets) {
    if ($all_dependencies{$_}) {
      delete $all_dependencies{$_}; # Remove it from the dependency list.
      my $warn_flag = 0;
      for (my $idx = 0; $idx < @explicit_dependencies; ++$idx) {
	if ($explicit_dependencies[$idx] == $_) { # Was it an explicit dependency?
	  splice(@explicit_dependencies, $idx, 1); # Remove it.
	  $warn_flag++ or
	    main::print_warning( $_, " depends on itself; circular dependency removed" );
	}
      }

    }
  }

#
# Make a list of the targets and the dependencies, first listing the explicitly
# specified ones, and then listing the implicit ones later.  It's confusing
# (and breaks some makefiles) if dependencies are built in a different order
# than they are specified.
#
  foreach (@explicit_targets) { delete $all_targets{$_}; }
  foreach (@explicit_dependencies) { delete $all_dependencies{$_}; }

  return ([ @explicit_targets, values %all_targets ],
	  [ @explicit_dependencies, values %all_dependencies ],
	  $command_string);
}

=head2 get_tagname

   $real_tag = $rule->get_tagname($tag)

Returns a unique tag name for $tag.
See L</"load_scaninfo">.

Since a rule can have multiple commands with the same include tag, the
requested tag might not match the actual tag that gets returned.

=cut
sub get_tagname {
  my ($self, $tag) = @_;
  return undef unless defined $tag;
  die if $tag eq "";
  my $real_tag = $tag;
  my $suffix = 0;
  while(exists $self->{META_DEPS}{$real_tag}) {
    $real_tag = $tag . ++$suffix;
  }
  $self->{META_DEPS}{$real_tag}={};
  return $real_tag;
}

=head2 add_include_dir

   $rule->add_include_dir($real_tag, $dirname);

Add an include directory for scanner caching.
See L</"load_scaninfo">.

$dirname is a name relative to build_cwd.

=cut
sub add_include_dir {
  my ($self, $tag, $dir) = @_;
  die unless $self->{META_DEPS}{$tag};
  die unless $tag;
  $self->{INCLUDE_PATHS}{$tag} ||= [];
  push(@{$self->{INCLUDE_PATHS}{$tag}}, $dir);
  $self->{INCLUDE_PATHS_REL}{$tag} = 1 unless defined $dir;
}

=head2 add_include_suffix

   $rule->add_include_suffix($real_tag, $suffix);

Add an include suffix list for scanner caching.
See L</"load_scaninfo">.

=cut
sub add_include_suffix {
  my ($self, $tag, $sfx) = @_;
  die unless $self->{META_DEPS}{$tag};
  $self->{INCLUDE_SFXS}{$tag} ||= [];
  push(@{$self->{INCLUDE_SFXS}{$tag}}, $sfx);
}

=head2 add_meta_dependency

   $rule->add_meta_dependency($real_tag, $src, $name, $finfo);

Add an include file for scanner caching.
Return value is TRUE iff the meta dependency failed to build.
See L</"load_scaninfo">.

$name is the requested name relative to the directory named by $src
(relative to the build directory).
If $real_tag is undef, then it is treated as a path including only ".".
$src is ignored if $name is an absolute path or none of the directories in
the include path are undefined.
If $src is undef, then the build directory is used.

The return value is zero on success, nonzero on build failure.

=cut
sub fix_file_ {
  my $name = shift;
  die unless defined($name) && $name ne "" && $name !~ m@^/+$@;
  $name =~ s@/+$@@;
  return $name;
}
sub add_any_dependency_ {
  my ($self, $key, $tag, $src, $name, $finfo) = @_;
  $tag="" unless defined $tag;
  # We won't care where the file was included from if it's not using a tag,
  # if the tag it's using doesn't care about the source directory, or if
  # the file was included with an absolute path.
  $src="" unless defined($src) && $tag ne "" &&
    $self->{INCLUDE_PATHS_REL}{$tag} && $name !~ m@^/@;
  $src=~s@/*$@/@ unless $src eq "";
  die unless $tag eq "" || $self->{META_DEPS}{$tag};
  $self->{$key}{$tag}{$src}{fix_file_($name)} = 1;
  if($finfo) {
    $self->add_dependency($finfo);
    return main::wait_for(main::build($finfo)) if $key eq "META_DEPS";
  }
  return 0; # success
}
sub add_any_dependency_if_exists_ {
  my ($self, $key, $tag, $src, $name, $finfo) = @_;

  # TBD: We should return undef if work would have to be done in order
  # to build or re-build $finfo. This would require the equivalent of the
  # GNU make -q option, which we don't have (yet).

  if($finfo && !$finfo->exists_or_can_be_built) {
	return undef;
  }
  local $MakeEvent::exit_on_error = 0;
  return !$self->add_any_dependency_($key, $tag, $src, $name, $finfo);
}
sub add_meta_dependency {
  my $self = shift;
  return $self->add_any_dependency_("META_DEPS", @_);
}

=head2 add_implicit_dependency

   $rule->add_implicit_dependency($real_tag, $src, $name, $finfo);

Add a dependency file for scanner caching.
See L</"load_scaninfo">.

This works just like add_meta_dependency, except that the rule doesn't
have to be re-scanned by virtue of an implicit dependency being out of date.

=cut
sub add_implicit_dependency {
  my $self = shift;
  $self->add_any_dependency_("IMPLICIT_DEPS", @_);
}

=head2 add_implicit_target

   $rule->add_implicit_target($name);

Add a target file for scanner caching.
See L</"load_scaninfo">.

=cut
sub add_implicit_target {
  my ($self, $name) = @_;
  $self->{IMPLICIT_TARGETS}{fix_file_($name)} = 1;
  my $finfo = file_info($name, $self->build_cwd);
  $self->add_target($finfo);
  return $finfo;
}

=head2 set_signature_method_scanner

   $rule->set_signature_method_scanner($name)

Like set_signature_method, except that it takes a class name instead of an
object, and it caches how the signature method was set by a scanner.

=cut
sub set_signature_method_scanner {
  my ($self, $name) = @_;
  eval "require Signature::$name" or
    die("failed to load class Signature::$name");
  my $signature = $ {"Signature::${name}::$name"} or
    die("invalid signature class $name");
  $self->{SIG_METHOD_NAME} ||= $name;
  $self->set_signature_method_default($signature);
  return $signature;
}

=head2 mark_scaninfo_uncacheable

   $rule->mark_scaninfo_uncacheable

Prohibit the rule from caching its scanner information.
This is useful when the information gathered by one of the scanners that
gets involved doesn't fit into the caching scheme, and that could cause
build inaccuracies.

=cut
sub mark_scaninfo_uncacheable {
  my $self = shift;
  $self->{SCANINFO_UNCACHEABLE} = 1;
}

=head2 cache_scaninfo

   $rule->cache_scaninfo(\@targets);

Set build_info_string's for scanner caching for each of @targets.
See L</"load_scaninfo">.

Once transferred to the build_info cache, the information is deleted from
$rule, in order to save memory.

=cut
sub clear_scaninfo_ {
  my ($self) = @_;

  for(
    "SIG_METHOD_NAME", "INCLUDE_PATHS", "INCLUDE_PATHS_REL", "INCLUDE_SFXS",
    "META_DEPS", "IMPLICIT_DEPS", "IMPLICIT_TARGETS",
  ) {
    delete $self->{$_};
  }
}
sub cache_scaninfo {
  my ($self, $targets) = @_;

  die if exists $self->{SCAN_FAILED};
  unless($main::nocache_scaninfo || $self->{SCANINFO_UNCACHEABLE}) {
    # Wipe out unnecessary info from IMPLICIT_DEPS:
    while(my ($tag, $th) = each %{$self->{IMPLICIT_DEPS}}) {
      while(my ($dir, $dh) = each %$th) {
	for my $name (keys %$dh) {
	  delete $dh->{$name} if exists $self->{META_DEPS}{$tag}{$dir}{$name};
	}
      }
    }

    # NOTE: Because the build info has to be valid at the next makepp run,
    # it is important that the directory/file paths do *not* have symbolic
    # links resolved, because otherwise there will be no hope of detecting
    # that we need to rebuild because a symbolic link was retargeted. The
    # exception to this rule is that the paths of directories containing the
    # file from which another file was included can be resolved, because the
    # dependency on the file that has the include directive will force a
    # rebuild if its path contains a retargeted symbolic link (and we are
    # able to detect that, which isn't the case yet).

    for my $tinfo (@$targets) {
      $self->save_build_info_($tinfo, "SIG_METHOD_NAME", sub {$_[0] || ""});
      $self->save_build_info_tag_($tinfo, "INCLUDE_PATHS", \&join1_);
      $self->save_build_info_tag_($tinfo, "INCLUDE_SFXS", \&join1_);
      $self->save_build_info_tag_($tinfo, "META_DEPS", \&join_dir_);
      $self->save_build_info_tag_($tinfo, "IMPLICIT_DEPS", \&join_dir_);
      $self->save_build_info_($tinfo, "IMPLICIT_TARGETS",
	sub { join1_([sort keys %{$_[0] || {}}]) }
      );
    }
    FileInfo::update_build_infos;
  }
  $self->clear_scaninfo_;
}
sub join1_ {
  return join("\01", map { defined($_) ? $_ : "" } @{$_[0]});
}
sub join_dir_ {
  my $hashref = shift || {};
  my @result;
  if($hashref->{""}) {
    push(@result, sort keys %{$hashref->{""}});
  }
  for my $dir (sort keys %$hashref) {
    if($dir ne "") {
      my @keys = sort keys %{$hashref->{$dir}};
      push(@result, $dir, @keys) if @keys;
    }
  }
  return join("\01", @result);
}
sub save_build_info_ {
  my ($self, $tinfo, $key, $sub) = @_;
  my $value = $self->{$key} if exists $self->{$key};
  $tinfo->set_build_info_string($key, &$sub($value));
}
sub save_build_info_tag_ {
  my ($self, $tinfo, $key, $sub) = @_;
  $self->save_build_info_($tinfo, $key,
    sub {
      my $hashref = $_[0] || {};
      my @result;
      for my $tag (sort keys %$hashref) {
	my $item=&$sub($hashref->{$tag});
	push(@result, join1_([$tag, $item])) if $item ne "";
      }
      return join("\02", @result);
    }
  );
}

=head2 load_scaninfo

   $rule->load_scaninfo($tinfo, $command_string, $explicit_dependencies);

Attempt to get all the scanned information about $rule from $tinfo's build
info file.
If this fails for whatever reason, including the scaninfo possibly being
out of date, then the reason is returned, and $rule is left in its original
state.
This typically means that it must be re-scanned from scratch.
Otherwise, 0 is returned.

=cut
sub split2_ {
  map {
    my @result = split(/\01/);
    push(@result, "") if /\01$/;
    \@result;
  } split(/\02/, shift);
}
sub load_scaninfo_single {
  my ($self, $tinfo_version, $tinfo, $command_string, $explicit_dependencies)
    = @_;

  # Fetch the info, or give up
  my $build_cwd_name = $tinfo_version->build_info_string("CWD");
  my $sig_method = $tinfo_version->build_info_string("SIG_METHOD_NAME");
  my $include_paths = $tinfo_version->build_info_string("INCLUDE_PATHS");
  my $include_sfxs = $tinfo_version->build_info_string("INCLUDE_SFXS") || "";
  my $meta_deps = $tinfo_version->build_info_string("META_DEPS");
  my $implicit_deps = $tinfo_version->build_info_string("IMPLICIT_DEPS");
  my $implicit_targets = $tinfo_version->build_info_string("IMPLICIT_TARGETS");
  return "info not cached" unless defined($build_cwd_name) &&
    defined($include_paths) &&
    defined($meta_deps) && defined($implicit_deps) &&
    defined($implicit_targets);

  # If the CWD changed, then we have to rescan in order to make sure that
  # path names in the scaninfo are relative to the correct directory.
  my $build_cwd = file_info($build_cwd_name, $tinfo->{".."});
  return "the build directory changed" unless $build_cwd eq $self->build_cwd;

  # Early out for command changed. This is redundant, but it saves a lot of
  # work if it fails, especially because loading the cached dependencies might
  # entail building metadepedencies.
  return "the build command changed"
    if $command_string ne $tinfo_version->build_info_string("COMMAND");

  my $saved_signature_method = $self->{SIGNATURE_METHOD};
  if($sig_method) {
    no strict 'refs';
    $self->set_signature_method_scanner($sig_method);
  }
  my @include_paths = split2_($include_paths);
  my @include_sfxs = split2_($include_sfxs);
  my @meta_deps = split2_($meta_deps);
  my @implicit_deps = split2_($implicit_deps);

  # Trump up a dummy scanner object to help us look for files.
  require Scanner;
  my $scanner = Scanner->new($self, ".");

  for(@include_paths) {
    my @path=@$_;
    my $tag=shift @path;
    for my $dir (@path) {
      $scanner->add_include_dir($tag, $dir eq "" ? undef : $dir);
    }
  }
  for(@include_sfxs) {
    my @sfxs=@$_;
    my $tag=shift @sfxs;
    for my $sfx (@sfxs) {
      $scanner->add_include_suffix($tag, $sfx);
    }
  }

  # Save the existing list of dependencies.
  my %saved_explicit_deps = %{$self->{ALL_DEPENDENCIES}};

  my %meta_dep_finfos; # Not used, but may be used in the future.
  my $cant_build_deps;
  {
    # Update $self to reflect the cached dependencies:
    my $key = "META_DEPS";
    TAG: for my $tag_record (@meta_deps, undef, @implicit_deps) {
      unless(defined $tag_record) {
	$key = "IMPLICIT_DEPS";
	next TAG;
      }
      my @incs = @$tag_record;
      die unless @incs;
      my $tag = shift @incs;
      $scanner->get_tagname($tag) if $tag ne ""; # make it a valid tag
      my $dir = $build_cwd;
      for my $item (@incs) {
	if($item =~ s@/$@@) {
	  $dir = file_info($item, $build_cwd);
	}
	else {
	  my $finfo;
	  if($tag ne "") {
	    $finfo = $scanner->find($tag, $item);
	    $self->add_any_dependency_if_exists_($key,
	      $tag, $dir->name($build_cwd), $item, $finfo
	    ) or $cant_build_deps=1;
	  }
	  else {
	    $finfo = file_info($item, $build_cwd);
	    $self->add_any_dependency_if_exists_($key,
	      undef, undef, $item, $finfo
	    ) or $cant_build_deps=1;
	  }
	  last TAG if $cant_build_deps;
	  $meta_dep_finfos{$finfo} = 1 if $finfo && $key eq "META_DEPS";
	}
      }
    }
  }

  # TBD: If you have a dependency in the target list, then it gets removed
  # after this method, but before the dependencies are stored in the build
  # info, and therefore the dependency list will look out of date when it
  # isn't. This only slows things down, only happens when the makefile is
  # screwy, and we can fix it using $explicit_dependencies.

  my @outdated;
  if(
    $cant_build_deps ||
    $self->signature_method->build_check(
      $tinfo_version,
      $self->sorted_dependencies([values %{$self->{ALL_DEPENDENCIES}}]),
      $command_string, $build_cwd, \@outdated
    )
  ) {
    # If @outdated is not empty, then that means that the *only* reason that
    # build_check returned TRUE is that the target is out of date with respect
    # to the dependencies listed therein. If none of them are meta
    # dependencies, then we don't need to re-scan.

    my $found = $cant_build_deps || !@outdated;
    unless($found) {
      for(@outdated) {
	if($meta_dep_finfos{$_}) {
	  $found=1;
	  last;
	}
      }
    }

    if($found) {
      # Reverse the side effects of this method:
      $self->{SIGNATURE_METHOD} = $saved_signature_method;
      %{$self->{ALL_DEPENDENCIES}} = %saved_explicit_deps;
      $self->clear_scaninfo_;

      return "couldn't build dependencies" if $cant_build_deps;
      return "meta dependencies are out of date";
    }
  }

  # Now we know that this is going to succeed, so go ahead and add the implicit
  # targets without fear of having to backtrack.
  for my $targ (split(/\01/, $implicit_targets)) {
    $self->add_implicit_target($targ);
  }

  return 0;
}
sub load_scaninfo {
  my $self=shift;
  my ($tinfo) = @_;

  return "--force_rescan option specified" if $main::force_rescan;

  my $msg;
  my $first_msg = $self->load_scaninfo_single($tinfo, @_);
  return 0 unless $first_msg;
  for my $existing_target (@{$tinfo->{ALTERNATE_VERSIONS} || []}) {
    my $name = $existing_target->name($self->build_cwd);
    $msg = $self->load_scaninfo_single($existing_target, @_);
    if($msg) {
      main::print_log(" Couldn't use " .
	$existing_target->name($self->build_cwd) .
	" to retrieve scan info because $msg");
    }
    else {
      main::print_log(" Using " .
	$existing_target->name($self->build_cwd) .
	" to retrieve scan info");
      return 0;
    }
  }
  return $first_msg;
}

sub sorted_dependencies {
  my ($self, $all_dependencies) = @_;
  my @sorted_dependencies;
  foreach my $dep (sort { $a->{NAME} cmp $b->{NAME} ||
			    $a->name cmp $b->name } @$all_dependencies) {
				# Get a sorted list of dependencies.  We need
				# to have these in a predictable order so
				# build info can be quickly compared.  It's
				# important to sort by the filename first
				# rather than the directories, because if we
				# use a repository, sometimes directories
				# change but filenames don't (if absolute
				# directory names are referenced in the build
				# procedure), and we want the system not to
				# force recompilation in this case.
    @sorted_dependencies && $sorted_dependencies[-1] == $dep and next;
				# Skip duplicates.
    push @sorted_dependencies, $dep;
  }
  return \@sorted_dependencies;
}

=head2 $rule->execute($command_string, $all_targets, $all_dependencies)

   $handle = $rule->execute($command_string, $all_targets, $all_dependencies);

Executes the given command string, and returns a handle for the executing
process (which may not have finished by the time we return).  The command
string may contain several shell commands separated by newlines.

This is part of the Rule class so the default execute routine can be
overridden.  You can replace the execute routine with any complicated
operation you want, provided it returns the appropriate kind of handles.

=cut

sub execute {
  my ($self, $actions, $all_targets, $all_dependencies) = @_;	# Name the arguments.

  if ($actions =~ /\brecursive_makepp\b/) { # Do we need to listen for
    main::setup_recursive_make_socket(); # recursive make?
  }

  my $build_cwd = $self->build_cwd;
#
# If we're not building in parallel, then we don't change the standard output
# of the commands.  In this case, if there's a directory change, we print
# it out before executing the commands.
#
  if (!$main::parallel_make) {
    print_build_cwd($build_cwd); # Make sure we notify user if we change
				# directories.
    return wait_for new MakeEvent::Process sub {
      $self->execute_command($build_cwd, $actions, $all_targets, $all_dependencies); # Execute the command.
    };
				# Wait for the process, because we don't want
				# to get ahead and start scanning files we
				# can't build yet.  That could mix up output
				# between make and the other process.
  }

#
# We're executing in parallel.	We can't print the directory name initially,
# and we have to redirect STDOUT, because we don't want to mix output from
# different commands.
#
# At present, we direct the output to a file, and then print it when we're
# done.
#
  else {
    my $tmpfile = "/tmp/makepp.$$" . substr rand, 1;
				# Make the name of a temporary file.
    my $proc_handle = new MakeEvent::Process sub {
#
# Child process.  Redirect our output to the temporary file and then start
# the build.
#
#      close STDIN;
# (Turns out we don't want to close STDIN, because then duping STDOUT causes
# fd 0 to be opened, rather than 2, so STDERR is actually file handle 0.  This
# caused a weird bug where error messages were not displayed on parallel
# builds.)
      close STDOUT;
      close STDERR;
      open(STDOUT, "> $tmpfile") ||
	die "$main::progname: can't redirect standard output to $tmpfile--$!\n";
      open(STDERR, ">&STDOUT") || die "can't dup STDOUT\n";
				# Make stderr go to the same place.
      $self->execute_command($build_cwd, $actions, $all_targets, $all_dependencies); # Execute the action(s).
    };

    when_done $proc_handle, sub {
#
# Gets executed in the parent process when the child has finished.
#
      local *JOB_OUTPUT;	# Make a local file handle.
      if (open(JOB_OUTPUT, $tmpfile)) { # Is there anything?
	print_build_cwd($build_cwd); # Display any directory change.
	print <JOB_OUTPUT>;	# Dump it all to STDOUT.
	close JOB_OUTPUT;
	unlink $tmpfile; # Get rid of it.
      }
      return 0;			# No error.
    }, ERROR => sub {
#
# Process exited with an error.	 Again, print out the program's output, but
# then exit with non-zero status.
#
      local *JOB_OUTPUT;	# Make a local file handle.
      if (open(JOB_OUTPUT, $tmpfile)) { # Is there anything?
	print_build_cwd($build_cwd); # Display any directory change.
	print <JOB_OUTPUT>;	# Dump it all to STDOUT.
	close JOB_OUTPUT;
	unlink $tmpfile; # Get rid of it.
      }
      $main::error_found = $_[0] # Remember the error status.  This will
	unless $main::keep_going; # cause us to stop compilation as soon as
				# possible.
      return $_[0];		# Propagate the error status.
    };
  }
}

=head2 $rule->print_command

Print out a command instead of executing it.  This is used only when we
are running with -n (dry run mode).

=cut
sub print_command {
  local $_ = $_[1];		# Access the command string.
  1 while s/^\s*([-\@]|noecho\s+|ignore_error\s+)//m;
				# Strip out any things that control printing,
				# because we always want to print with -n.
  print "$_\n";
  return undef;			# No build handle.
}

=head2 $rule->append($rule)

Add the targets and commands from another rule to this rule.  This is used
only when parsing double colon rules like this:

   clean::
       $(RM) *.o

   # Later in the makefile:
   clean::
       $(RM) y.tab.c y.tab.h

=cut
sub append {
    my ($self, $other) = @_;	# Name the arguments.

    $self->{DEPENDENCY_STRING} .= " $other->{DEPENDENCY_STRING}";
    $self->{COMMAND_STRING} .= $other->{COMMAND_STRING};
    $self->{RULE_SOURCE} .= ",$other->{RULE_SOURCE}";
}

# Returns the action split on newline, with prefixes (the stuff that doesn't
# get executed by the sub-shell) stripped off. Each entry is a 3-element
# arraryref whose first element is the command, whose second element is
# the perl command, and whose third element is the flags.
sub split_actions {
  my $actions=$_[0];
  my @actions;
  while ($actions) {
    $actions =~ s/^((?:[\@-]|noecho\s+|ignore_error\s+)*)(?:(?:make)?perl\s*(\{(?s:\{.*?\}\})?.*)|(.*))\n?//m;
    my ($flags, $perl, $action) = ($1, $2, $3);
    push(@actions, [$action, $perl, $flags]);
  }
  return @actions;
}

=head2 $rule->setup_environment()

Sets %ENV. No mechanism for restoring the previous environment is provided.
This might do other similar things (I<e.g.> set the umask) in the future.

=cut
sub setup_environment {
  my $self = shift;
  my ($exports, $environment) = @{$self->{MAKEFILE}}{qw(EXPORTS ENVIRONMENT)};

  %ENV = %$environment;		# Set the environment.
#
# Handle any exported variables.
#
  if ($exports) {		# Any exported variables?
    my ($var, $val);
    while (($var, $val) = each %$exports) {
      $ENV{$var} = $val;
    }
  }
}

#
# The following internal subroutine executes each action.  Arguments:
# a) The directory that we're supposed to execute this from.
# b) The lines of actions to execute.
# c) A hash of variables that must be exported to the environment, and their
#    values.
# d) The environment to use (not including the exports).
#
# At this point, STDOUT and STDERR should be set up to wherever the command
# is supposed to output to.
#
sub execute_command {
  my ($self, $build_cwd, $actions, $all_targets, $all_dependencies) = @_; # Name the arguments.

  chdir $build_cwd;		# Move to the correct directory.
  $self->setup_environment;

  $main::recursive_make_socket_name and	# Pass info about recursive make.
    $ENV{'MAKEPP_SOCKET'} = $main::recursive_make_socket_name;

#
# Now execute each action.  We exec the action if it is the last in the series
# and we don't need to ignore the return code; otherwise, we call system()
# instead.  This means we leave an extra process lying around in those cases,
# but it's too tricky to avoid it in such comparatively rare situations.
#
  my @actions = split_actions($actions); # Get the commands to execute.

  while(@actions) {
    my $action_rec=shift(@actions);
    my ($action, $perl, $flags) = @$action_rec;

#
# Parse the @ and - in front of the command.
#
    my $silent_flag = $main::silent_execution || $flags =~ /\@|noecho/;
    my $error_abort = $flags !~ /-|ignore_error/;

    $| = 1;			# Flush immediately.  Otherwise, with perl
				# 5.005, the action is never printed for
				# some reason.

    if (defined $perl) {
      print "perl $perl\n" unless $silent_flag;
				# This prints out makeperl as perl.  That is
				# correct, because it has already been expanded
				# to plain perl code.
     local $Makesubs::rule = $self;
      local $self->{EXPLICIT_TARGETS} = $all_targets;
      local $self->{EXPLICIT_DEPENDENCIES} = $all_dependencies;
      eval "no strict; package $self->{MAKEFILE}{PACKAGE};\n$perl";
      if ($@) {
	print STDERR 'perl: '.$@;
	return 1 if $error_abort;
      }
      next;			# Process the next action.
    }

    print "$action\n" unless $silent_flag;
    if (!$main::can_fork) {	# Can't fork or exec on windows.
      system(TextSubs::format_exec_args($action));
      $error_abort and $? and return ($?/256) || 1; # Quit if an error.
      next;			# Process the next action.
    }

    if ($error_abort && !@actions) { # Is this the last action?
      $SIG{__WARN__} = sub {};	# Suppress annyong warning message here if the
				# exec fails.
      exec(TextSubs::format_exec_args($action));
				# Then execute it and don't return.
				# This discards the extra make process (and
				# probably saves lots of memory).
      die "exec $action failed--$!\n"; # Should never get here.
    }
    else {			# We have more to do after the process
				# finishes, so we need to keep running
				# after the process starts.
#
# We used to do with with system(), but system has two serious drawbacks:
# 1) A nasty message is printed if the first word is not a valid shell
#    command, and the perl process aborts, apparently without setting the
#    correct exit code.
# 2) On linux, system() blocks signals, which means that makepp wouldn't
#    respond to ^C or other things like that.  (See the man page.)
#
      my $pid = fork();
      unless ($pid) {		# Executed in child process:
	$SIG{__WARN__} = sub {}; # Suppress annoying warning message here if
				# the exec fails.
	exec(TextSubs::format_exec_args($action));
	die "exec $action failed--$!\n"; # Should never get here.
      }

      $pid == -1 and die "fork failed--$!\n";
      wait;			# Wait for it to finish.
      $error_abort && $? and exit(($?/256) || 1);
				# Quit on an error.  $status/256 is the exit
				# status of the process, but if the system()
				# call failed for some reason, that may be 0.
    }
  }

  $main::can_fork or return 0;	# If we didn't fork, we'll always get here.

  exit 0;			# If we get here, it means that the last
				# command in the series was executed with
				# ignore_error.
}

#
# This subroutine prints out a message if we've changed directories.  This
# message is important for programs like emacs that look through the compiler
# output--it helps them find the directory that the file containing the errors
# is located in.
#
sub print_build_cwd {
  my $build_cwd = $_[0];
  if (!$Rule::last_build_cwd ||	# No previous cwd?
      $Rule::last_build_cwd != $build_cwd) { # Different from previous?
    print "$main::progname: Leaving directory `" . $Rule::last_build_cwd->absolute_filename . "'\n"
      if $Rule::last_build_cwd;	# Don't let the directory stack fill up.
    print "$main::progname: Entering directory `" . $build_cwd->absolute_filename . "'\n";
    $Rule::last_build_cwd = $build_cwd;
  }
}


=head2 $rule->signature_method

  my $sig_method = $rule->signature_method;

Returns the signature method to use to determine whether this rule needs to
be applied.

The default method is exact_match (see Signature::exact_match for details).
However, if the name of the target is Makefile or Makefile.in, then the
default method is target_newer.	 This is to allow legacy makefiles to work.

=cut

sub signature_method {
  my $self = $_[0];

  $self->{SIGNATURE_METHOD} and return $self->{SIGNATURE_METHOD};

  return $main::default_signature_method;
				# Use the default method.
}

sub set_signature_method {
  $_[0]->{SIGNATURE_METHOD} = $_[1];
}

#
# This subroutine sets the signature method if one hasn't already been
# specified.
#
sub set_signature_method_default {
  $_[0]->{SIGNATURE_METHOD} ||= $_[1];
}

=head2 $rule->scan_action

  $rule->scan_action($action_string);

Scans the given actions, looking for additional dependencies (such as include
files) or targets (such as .libs/xyz.lo) that weren't explicitly listed.  Make
variables in the action string have already been expanded.

Calls $rule->add_dependency for new dependencies, and $rule->add_target for
new targets.

=cut
sub scan_action {
  my ($self, $action) = @_;	# Name the arguments.

  $self->parser()->parse_rule($action, $self);
}

=head2 $rule->parser

  $rule->parser()

Returns the rule scanner object, creating it if it doesn't already exist.

=cut
sub parser {
  my $self=shift;
  my $parser_obj = $self->{PARSER_OBJ}; # Is the scanner already cached?
  unless($parser_obj) {
    my $scanner = $self->{ACTION_SCANNER}; # Was one explicitly set?
    if($scanner) {
      $parser_obj=&$scanner(); # Yes: generate it
    }
    else {
      $parser_obj = new ActionParser; # No: Use the default
    }
    $self->{PARSER_OBJ}=$parser_obj;
  }
  return $parser_obj;
}

=head2 $rule->source

  $source_string = $rule->source;

Returns a description of where this rule was encounteredq, suitable for error
messages.

=cut
sub source {
  return $_[0]->{RULE_SOURCE};
}

#
# This subroutine is called to add a possibly new dependency to the list of
# all dependencies for the current rule.
#
# Arguments:
#	$rule->add_dependency($object_info);
#
# This should only be called from subroutines which are called by
# find_all_targets_dependencies.  This basically means it should only be
# called by the command scanners.
#
sub add_dependency {
  $_[0]->{ALL_DEPENDENCIES}{$_[1]} ||= $_[1];
				# Store it if we didn't already know about it.
}

#
# This subroutine is called to add a possibly new target to the list of targets
# for the current rule.
#
# Arguments:
#     $rule->add_target($object_info);
#
# This should only be called from subroutines which are called by
# find_all_targets_dependencies.  This basically means it should only be
# called by the command scanners.
#
sub add_target {
  my ($self, $oinfo) = @_;

  return if $self->{ALL_TARGETS}{$oinfo}; # Quit if we already knew about this.

#
# This target may have dependencies which were explicitly listed in some other
# part of the makefile, as used to be common for specifying include files.
# For example, we might have a rule like:
#
#   %.o: %.c
#     compilation commnad
#
#   xyz.o : abc.h def.h ghi.h
#
  if ($oinfo->{ADDITIONAL_DEPENDENCIES}) { # Any additional explicit dependencies?
    foreach (@{$oinfo->{ADDITIONAL_DEPENDENCIES}}) {
      my ($dep_str, $makefile, $makefile_line) = @$_;
				# Name the components of the stored info.
      foreach my $depname (split_on_whitespace($makefile->expand_text($dep_str, $makefile_line))) {
	my @dep_infos;
	if ($depname =~ /[\[\*\?]/) {		# Is it a wildcard?
	  @dep_infos = Glob::zglob_fileinfo(unquote($depname), $makefile->{CWD});
				# Get which files this is referring to.
	} else {
	  @dep_infos = (main::find_makepp_info(unquote($depname), $makefile->{CWD}));
	}

	foreach my $dep (@dep_infos) { $self->add_dependency($dep); }
				# Add them to the dependency list.
	push @{$self->{EXTRA_DEPENDENCIES}}, @dep_infos;
      }
    }
  }

  $self->{ALL_TARGETS}{$oinfo} = $oinfo;
}

###############################################################################
#
# This is a replacement for the Rule when there is no actual rule specified
# in the file.	Most of the subroutines in this package are dummies to
# replace the functionality in the Rule class.
#
package DefaultRule;

use TextSubs;

#
# Establish a default rule for a new file:
#
sub new {
  my ($class, $target) = @_;
				# Name the arguments.

  return bless { TARGET => $target }, $class;
				# Make the rule object.	 Declare it local
				# so it's accessible to any subroutines that
				# Makefile::expand_text calls.
}

sub build_cwd {
  return $FileInfo::CWD_INFO;
}

sub find_all_targets_dependencies {
  my $self = shift @_;
  my $target = $self->{TARGET};	# Get the target object info.
  if ($target->{ADDITIONAL_DEPENDENCIES}) {
				# Are there dependencies for this file even
				# though there's no rule?
				# We have to expand the dependency list now.
    my @addl_deps;
    foreach (@{$target->{ADDITIONAL_DEPENDENCIES}}) {
      my ($dep_str, $makefile, $makefile_line) = @$_;
				# Name the components of the stored info.
      foreach my $depname (split_on_whitespace($makefile->expand_text($dep_str, $makefile_line))) {
	if ($depname =~ /[\[\*\?]/) {		# Is it a wildcard?
	  push(@addl_deps,
	       Glob::zglob_fileinfo(unquote($depname), $makefile->{CWD}));
				# Get which file(s) this is referring to.
	} else {
	  push @addl_deps, main::find_makepp_info(unquote($depname),
						  $makefile->{CWD});
	}
      }
    }

    return ([$target], \@addl_deps, "");
  }

  return ([ $target ], [], "");
}

#
# If we ever get to this subroutine, it means there's no way to build this
# file.
#
sub execute {
  my $self = $_[0];

  return MakeEvent::when_done sub {
    my $target = $self->{TARGET};
    if ($target->{ADDITIONAL_DEPENDENCIES}) {
				# If it has additional dependencies, then
				# there was a rule, or at least we have to
				# pretend there was.
      $target->may_have_changed; # It's possible that some other command
				# made the target while we weren't looking.
				# Some makefiles are written that way.
      $target->file_exists and return 0;
				# If the file doesn't exist yet, we may
				# have to link it from a repository.
      if ($target->{ALTERNATE_VERSIONS}) {
	$target->move_or_link_target($target->{ALTERNATE_VERSIONS}[0], 0);
				# Link it in from the repository.
      }
      return 0;			# Return success even if the file doesn't exist.
				# This is to handle things like
				# FORCE:
				# which are really just dummy phony targets.
    }
    main::print_error( 'No rule to make ', $target );
    return -1;			# Return nonzero to indicate error.
  };
}

#
# This gets called whenever we access a file for which there is no rule
# when we are running with -n.	In this case we don't want to do anything.
#
sub print_command {
  my $target = $_[0]->{TARGET};
  if ($target->{ADDITIONAL_DEPENDENCIES}) {
    return 0;			# Handle case like
				# all: xyz
				# where there is no rule for all.
  }
  main::print_error( 'No rule to make ', $target );

  return -1;			# Return nonzero to indicate error.
}

#
# The signature method that we use causes the file to be remade if it doesn't
# exist, and leaves it alone if it does.
#
sub signature_method {
  return $Signature::default_rule::default_rule;
}

sub source { return "default rule" };

#
# This package implements signature checking for files for which there is no
# rule.	 We "rebuild" only if the file does not exist.	This will cause the
# file to be linked in from a repository if we can find it somewhere.
#

@Signature::default_rule::ISA = qw(Signature::exact_match);
				# Inherit the check_move_or_link function.

$Signature::default_rule::default_rule = bless {}; # Make the singleton object.

sub signature {
  return $_[1]->signature;
}

sub build_check {
  shift @_;			# Get rid of dummy argument.
  my $tinfo = $_[0];

  if($tinfo->is_stale) {
    main::print_warning($tinfo->absolute_filename .
      " is a stale file generated by makepp,\n" .
      "but there is no rule to build it any more.  Makepp assumes that it\n" .
      "is now a source file.  If this is correct, then you should add it\n" .
      "to your source control system (if any), and touch the file to make\n" .
      "this warning go away.  Otherwise, you should remove the file,\n" .
      "because the targets that depend on it will not be reproducible.\n"
    );
  }

  # It would be faster to check for existence first, but that doesn't work,
  # because then if the file were from a repository but the repository version
  # changed, then the file would originally exist, and then it gets deleted
  # by build_info_string(), which returns undef, making it look as though the
  # file is up-to-date, which in fact it doesn't even exist!
  return defined($tinfo->build_info_string("FROM_REPOSITORY")) ||
    !$tinfo->file_exists;
}

sub sorted_dependencies {
  return $_[0]->Rule::sorted_dependencies($_[1]);
}

# If there is no action, then there is nothing to scan, so presumably nothing
# was affected by scanning.
sub cache_scaninfo { }

1;
