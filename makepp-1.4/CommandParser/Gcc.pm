=head1 NAME

CommandParser::Gcc - makepp command parser for Gcc

=head1 DESCRIPTION

Scans a gcc compile command for implicit dependencies.

This class is readily subclassed for similar things, such as the
C preprocessor.

=cut

use strict;
package CommandParser::Gcc;

use CommandParser;
use vars qw/@ISA/;
BEGIN { @ISA = qw/CommandParser/; }

use TextSubs;
use FileInfo;
use FileInfo_makepp;

sub new {
	my $self=shift;
	my $class=ref($self) || $self;
	$self=$self->SUPER::new(@_);
	require Scanner::C;
	$self->{SCANNER}=new Scanner::C($self->rule, $self->dir);
	return bless $self, $class;
}

sub set_default_signature_method {
	my $self = shift;
	my ($leave_comments) = @_;

	# Use the MD5 signature checking when we can.
	$main::has_md5_signatures and
	  $self->rule->set_signature_method_scanner(
	    $leave_comments ? "md5" : "c_compilation_md5"
	  );
}

# The subclass can override this. Don't start doing any actual scanning here,
# because the signature method isn't necessarily set yet.
sub parse_arg {
	my $self=shift;
	local $_ = shift;
	my ($words, $files)=@_;

	if(/^-/) {
		# Ignore unknown option.
	}
	elsif(TextSubs::is_cpp_source_name($_)) {
		push(@$files, $_);
	}
}

sub xparse_command {
	my $self=shift;
	my ($command, $setenv)=@_;

	my @words=@$command;
	my $dir=$self->dir;
	my $scanner=$self->{SCANNER};

	$scanner->should_find("user");
	$scanner->info_string("user", "INCLUDES");
	$scanner->info_string("sys", "SYSTEM_INCLUDES");
	$scanner->add_include_suffix("lib", ".la");
	$scanner->add_include_suffix("lib", ".so");
	$scanner->add_include_suffix("lib", ".sa");
	$scanner->add_include_suffix("lib", ".a");

	my @prefiles;
	my @files;
	my $idash; # already saw -I-
	my @incdirs;
	my @inc1dirs; # specified *before* -I-
	my @libs;
	my $stop_at_obj; # saw -c
	my @obj_files;
	my $leave_comments; # specified -C
	my $nostdinc; # specified -nostdinc
	my $traditional;

 	my $cmd = shift @words; # Get rid of command itself
	$cmd =~ s@.*/@@;
 	my $gnu = 1 if $cmd =~ /^g/i;
	while(defined($_ = shift @words)) {
		if(/^-I(.*)$/) {
			my $dir = $1 || shift @words;
			if($dir eq "-") {
				$idash=1;
			}
			elsif($idash) {
				push(@incdirs, $dir);
			}
			else {
				push(@inc1dirs, $dir);
			}
		}
		if(/^-L(.*)$/) {
			my $dir = $1 || shift @words;
			$scanner->add_include_dir("lib", $dir);
		}
		if(/^-l(.+)$/) {
			push(@libs, $1);
		}
		if($_ eq "-c") {
			$stop_at_obj = 1;
		}
		elsif(/^-D(.*)$/) {
			local $_ = $1 || shift @words;
			if(/^(\w+)=(.*)$/) {
				$scanner->set_var($1, $2);
			}
			else {
				$scanner->set_var($_, "");
			}
		}
		elsif(/^-U(.*)$/) {
			my $var = $1 || shift @words;
			$scanner->set_var($var, undef);
		}
		elsif($_ eq "-C") {
			$leave_comments=1;
		}
		elsif($_ eq "-o") {
			$self->add_target(shift @words);
		}
		elsif($_ eq "-include" || $_ eq "-imacros") {
			push(@prefiles, shift @words);
		}
		elsif($_ eq "-nostdinc") {
			$nostdinc=1;
		}
		elsif($_ eq "-(no-)?gcc") {
			$gnu = !$1;
		}
		elsif($_ eq "-traditional") {
			$traditional = 1;
		}
		elsif(/\.(?:o|lo|la|a|sa|so|so\.*)$/) { # object file?
			if (/[\*\?\[]/) { # wildcard?
 # TBD: Why is this disabled?
 if(0) {
				require Glob;
				push @obj_files,
				  Glob::zglob($_, $self->dirinfo);
 }
			} else {
				push @obj_files, $_;
			}
		}
		else {
			$self->parse_arg($_, \@words, \@files);
		}
	}

	$self->set_default_signature_method($leave_comments);

	unless($idash) {
		unshift(@incdirs, @inc1dirs);
		@inc1dirs=();
		$scanner->add_include_dir("user", undef);
	}
	foreach (@inc1dirs, @incdirs) {
		$scanner->add_include_dir("user", $_);
	}
	foreach (@incdirs) {
		$scanner->add_include_dir("sys", $_);
	}
	if($nostdinc) {
		$scanner->should_find("sys");
	}
	else {
		require Makesubs;
		foreach (@Makesubs::system_include_dirs) {
			foreach my $tag ("user", "sys") {
				$scanner->add_include_dir($tag,
				  $_->absolute_filename
				);
			}
		}
	}

	for(@prefiles) {
		$scanner->scan_file($self, "c", $_) or return undef;
	}
	$scanner->set_var("__STDC__", "") unless $traditional;
	$scanner->set_var("__GNUC__", "") if $gnu;
	my $context = $scanner->get_context;
	for(@files) {
		$scanner->reset($context);
		$scanner->set_var("__cplusplus", "") if /\.(cc|cxx|cpp)$/i ||
		  ($FileInfo::case_sensitive_filenames && /\.C$/);
		$scanner->scan_file($self, "c", $_) or return undef;
	}

	unless($stop_at_obj) {
		foreach my $libname (@libs) {
			$scanner->add_dependency($self, "lib", "lib$libname");
		}
		for(@obj_files) {
			$self->add_simple_dependency($_);
		}
	}
	return 1;
}

1;
