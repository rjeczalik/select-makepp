=head1 NAME

CommandParser::Basic - The simplest possible makepp command parser

=head1 DESCRIPTION

Does nothing except add a dependency on the executable if it has a relative
path, which is implemented in the base class.

=cut

use strict;
package CommandParser::Basic;

use CommandParser;
use vars qw/@ISA/;
BEGIN { @ISA = qw/CommandParser/; }

sub xparse_command { 0 }

1;
