#!/usr/bin/perl -w
#
# This script asks the user the necessary questions for installing
# makepp.
# $Id: install.pl,v 1.26.2.1 2004/05/09 16:25:26 pfeiffer Exp $
#

use Config;
use File::Copy;

#
# First make sure this version of perl is recent enough:
#
eval { require 5.00503; };
if ($@) {			# Not recent enough?
  die "I need perl version 5.005_03 or newer.  If you have it installed
somewhere already, run this installation procedure with that perl binary, e.g.,

	perl5.005 install.pl

If you don't have a recent version of perl installed (what kind of system are
you on?), get the latest from www.perl.com and install it.
";
}

$perlbin = $Config{'perlpath'};

print "Using perl in $perlbin.\n";

#
# Load the version number so it can be automatically inserted into the
# files.
#
open(VERSION, "VERSION") || die "You are missing the file VERSION.  This should be part of the standard distribution.\n";
$VERSION = <VERSION>;
chomp $VERSION;
close VERSION;

#
# Now figure out where everything goes:
#
$prefix = "/usr/local";

$bindir = shift(@ARGV) ||
  read_with_prompt("
Makepp needs to know where you want to install it and its data files.
makepp is written in perl, but there is no particular reason to install
any part of it in the perl hierarchy; you can treat it as you would a
compiled binary which is completely independent of perl.

Where should the makepp executable be installed [$prefix/bin]? ") ||
  "$prefix/bin";

$bindir =~ m@^(.*)/bin@ and $prefix = $1;
				# See if a prefix was specified.

$datadir = shift @ARGV || read_with_prompt("
Makepp has a number of library files that it needs to install somewhere.  Some
of these are perl modules, but they can't be used by other perl programs, so
there's no point in installing them in the perl modules hierarchy; they are
simply architecture-independent data that needs to be stored somewhere.

Where should the library files be installed [$prefix/share/makepp]? ") ||
  "$prefix/share/makepp";
use vars qw/$usedatadir/;
if ($datadir !~ /^\//) {	# Make a relative path absolute.
  use Cwd;
  my $cwd = cwd;
  chdir $datadir;
  $usedatadir = "use lib (\$datadir = '" . cwd . "');\n";
  chdir $cwd;
} else {
  $usedatadir = "use lib (\$datadir = '$datadir');\n";
}

$mandir = shift @ARGV || read_with_prompt("
Where should the manual pages be installed?
Enter \"none\" if you do not want the manual pages.
Man directory [$prefix/man]: ") ||
  "$prefix/man";

$htmldir = shift @ARGV || read_with_prompt("
Where should the HTML documentation be installed?
Enter \"none\" if you do not want any documentation installed.
HTML documentation directory [$prefix/share/makepp/html]: ") ||
  "$prefix/share/makepp/html";

use vars qw/$findbin/;
$findbin = shift @ARGV;
defined($findbin) or $findbin = read_with_prompt("
Where should the library files be sought relative to the executable?
Enter \"none\" to seek in $datadir [none]: ") || "none";
$findbin=0 if $findbin eq "none";
if($findbin) {
  $usedatadir = "use FindBin;\n" .
    qq{use lib (\$datadir = "\$FindBin::RealBin/$findbin");};
}

@sig_num{split ' ', $Config{sig_name}} = split ' ', $Config{sig_num};
$USR1 = $sig_num{USR1}; $USR1 = $USR1; 	# suppress used-only-once warning

substitute_file("makepp", $bindir, 0755);
substitute_file("makeppclient", $bindir, 0755);
substitute_file("recursive_makepp", $datadir, 0644);

make_dir("$datadir/Signature");
make_dir("$datadir/Scanner");
make_dir("$datadir/CommandParser");
make_dir("$datadir/ActionParser");
foreach $module (qw(FileInfo FileInfo_makepp MakeEvent Glob Makefile Makesubs Rule
                    ActionParser ActionParser/Legacy ActionParser/Specific
                    CommandParser CommandParser/Basic CommandParser/Gcc
		    CommandParser/Vcs
                    Scanner Scanner/C Scanner/Vera Scanner/Verilog
		    Signature TextSubs
		    Signature/exact_match Signature/target_newer
		    Signature/c_compilation_md5 Signature/md5
		    Signature/verilog_simulation_md5
		    Signature/verilog_synthesis_md5)) {
  copy("$module.pm", "$datadir/$module.pm");
  chmod 0644, "$datadir/$module.pm";
}

foreach $include (qw(c_compilation_md5 infer_objects
		     makepp_builtin_rules makepp_default_makefile)) {
  copy("$include.mk", "$datadir/$include.mk");
  chmod 0644, "$datadir/$include.mk";
}

#
# From here on we treat the pod files
#
chdir 'pod';

#
# Install the man pages:
#
if ($mandir ne 'none') {
  make_dir("$mandir/man1");
  foreach $file (glob("*.pod")) {
    my $manfile = $file;
    $manfile =~ s/\.pod$/.1/;   # Get the name of the man file.
    $manfile =~ s@^pod/@@;
    system("pod2man $file > $mandir/man1/$manfile 2>/dev/null");
                                # Ignore stderr because older versions of
                                # pod2man (e.g., perl 5.006) don't understand
                                # =head3.
    chmod 0644, "$mandir/man1/$manfile";
  }
}

#
# Now massage and install the HTML pages.
#
use Pod::Html ();

sub highlight_keywords() {
  s!\G(\s+)((?:noecho\s+|ignore_error\s+|makeperl\s+|perl\s+|[-\@])+)!$1<b>$2</b>! or

  s!\G(define|ifn?def|makesub|sub)(&nbsp;| +)([-.\w]+)!<b>$1</b>$2<i>$3</i>! or
  s!\G(register_scanner|signature)(&nbsp;| +)([-.\w]+)!<b>$1</b>$2<u>$3</u>! or
  # repeat the above, because they may appear in C<> without argument
  s!\G(else|endd?[ei]f|export|fi|ifn?eq|[_-]?include|load[_-]makefile|makeperl|no[_-]implicit[_-]load|perl(?:|[_-]begin|[_-]end)|repository|unexport|define|ifn?def|makesub|sub|register_scanner|signature)\b!<b>$1</b>! or

    # highlight assignment
    s|\G(\s*(?:[-.\w\s%*?\[\]]+:\s*)?)([-.\w]+)(?= *[:+?!]?=)|$1<i>$2</i>|;

  # highlight rule options
  s!(: *)(quickscan|scanner|signature|smartscan)(&nbsp;| +)([-\w]+)!$1<b>$2</b>$3<u>$4</u>! or
  # repeat the above, because they may appear in C<> without argument
  s!(: *)(foreach|quickscan|scanner|signature|smartscan)\b!$1<b>$2</b>!;
}

sub highlight_variables() {
  s((\$[\{\(])([-\w]+)){
    my( $prefix, $name ) = ($1, $2);
    $name = "<b>$name</b>" if
      $name =~ /absolute[_-]filename|
	add(?:pre|suf)fix|
	basename|
	CURDIR|
	dependenc(?:y|ies)|
	dir(?:[_-]noslash)?|
	filesubst|
	filter(?:[_-]out)?|
	find(?:[_-](?:program|upwards)|file|string)|
	first(?:[_-]available|word)|
	foreach|
	if|
	infer[_-](?:linker|objects)|
	inputs?|
	join|
	make(?:perl)?|
	notdir|
	only[_-](?:generated|stale|(?:non)?targets)|
	origin|
	outputs?|
	patsubst|
	perl|
	phony|
	print|
	PWD|
	relative[_-](?:filename|to)|
	shell|
	sort(?:ed_dependencies|ed_inputs)?|
	stem|
	strip|
	subst|
	suffix|
	targets?|
	wildcard|
	word(?:list|s)|
	xargs/x;
    "$prefix<i>$name</i>"
  }eg;
}

if ($htmldir ne 'none') {
  my $tmp = '/tmp/makepp' . substr rand, 1;
  make_dir($htmldir);
  $htmldir =~ s!^([^/])!../$1!;
  for( <*.pod> ) {
    Pod::Html::pod2html qw'--libpods=makepp_functions:makepp_rules:makepp_statements:makepp_variables:makepp_signatures
			   --podpath=. --htmlroot=. --infile', $_, '--outfile', $tmp;
    # not available in 5.005_03: --css=makepp.css
    open TMP, $tmp;
    s/pod$/html/;
    open STDOUT, ">$htmldir/$_" or die "$!";
    chmod 0644, "$htmldir/$_";
    my $index;
    while( <TMP> ) {
      print, next if $Pod::Html::VERSION < 1.03; # Perl 5.005
      s/<(\/?\w+)/<\L$1/g if $Pod::Html::VERSION < 1.04; # Perl 5.6

      if ( /^<\/head>/../<h1>.*(?:DESCRIPTION|SYNOPSIS)/ ) {
	if ( /<(?:\/?ul|li)>/ ) {
	  # These are visible anyway when the index is.
	  next if /NAME|DESCRIPTION|SYNOPSIS|AUTHOR/;
	  $index .= $_;
	}
	# Such a line MUST be present in every POD.
	next unless s!<p>(makepp\w*) -- (.+)!$2!;
	my $title = uc $1;
	# Paragraph maybe not complete, grab rest
	$_ .= <TMP> while ! s!</p>$!</b>!i;
	my $img = '<img src="makepp.gif" width=142 height=160 title=makepp align=right border=0>';
	$title =~ tr/_/ /;
	for ( $index ) {
	  # Rearrange the index, because we threw out some items, discard it if empty.
	  s!<ul>\s+</ul>!!;
	  m!<ul>\s+<ul>! &&
	  s!</ul>\s+</ul>!</ul>! &&
	  s!<ul>\s+<ul>!<ul>!;
	}
	$_ = "<link rel=stylesheet href='makepp.css' type='text/css'>
<link rel=icon href='url.png' type='image/png'>
</head><body><h1>" .
	  ((length($title) > 6) ? "<a href='.'>$img</a>" : $img) .
	  "$title</h1>\n<p><b>$_</p>$index";
      } elsif ( s/<pre>\n/<pre>/ ) {
	$pre = 1;
      } elsif ( $pre ) {
	# unindent initial whitespace which marks <pre> in pod
	s/^ {1,7}\t(\t*)(?!#)/$1    / or s/^    ?//;

	# don't parse comments
	$end = s/(#(?: .*?)?)((?:<\/pre>)?)$// ? "<span class=comment>$1</span>$2" : '';

	s!^(%? ?)makepp\b!$1<b>makepp</b>!g or	# g creates BOL \G for keywords
	  highlight_keywords;
	highlight_variables;

	# put comment back in
	s/$/$end/ if $end;
	$pre = 0 if m!</pre>!;
      } elsif ( /^<\/dd>$/ ) { # repair broken nested =over
	$dd = 1;
	next;
      } elsif ( $dd and /^<(?:d[dt]|\/dl)>/ ) {
	$dd = 0;
	$_ = "</dd>$_";
	s!"item_(\w+)[^"]*">\1!"$1">$1!;	# fix =item anchor
	s!"item_(_\w+)">!"$1">!;		# fix =item hexcode-anchor
	s!"item_(%[%\w]+)">!(my $i = $1) =~ tr/%/_/; "'$i'>"!e; # Perl 5.6
      } else {
	s!<li></li>!<li>!;
	s!"item_(\w+)[^"]*">\1!"$1">$1!;	# fix =item anchor
	s!"item_(_\w+)">!"$1">!;		# fix =item hexcode-anchor
	s!#item_(\w+)">!#$1">!g;		# fix =item link
	s!"item_(%[%\w]+)">!(my $i = $1) =~ tr/%/_/; "'$i'>"!e; # Perl 5.6
	s!#item_(%[%\w]+)">!(my $i = $1) =~ tr/%/_/; "#$i\">"!ge; # Perl 5.6
	highlight_keywords while /<code>/g;	# g creates "pseudo-BOL" \G for keywords
	highlight_variables;
	if ( /<h1.+AUTHOR/ ) {	# Put signature at bottom right.
	  $_ = <TMP>;
	  s/p>/p align=right>/i;
	}
      }
      s!\./\./makepp!makepp!g;
      print;
    }
  }

  unlink $tmp;

  for( qw'makepp.gif makepp.css pre.png url.png makepp_compatibility.html' ) {
    copy $_, "$htmldir/$_";
    chmod 0644, "$htmldir/$_";
  }

  symlink 'makepp.html', "$htmldir/index.html";
}

#
# Figure out whether we need to do anything about Digest::MD5.
#
eval "use Digest::MD5";		# See if it's already installed.
if ($@) {
  print "\nIt looks like you don't have the perl module Digest::MD5 installed.
Makepp likes to have Digest::MD5 installed because it is a better technique
for checking whether files have changed than just looking at the dates.
Makepp will work without it, however.
   If you would like this feature, you'll have to install this perl module.
If you installed perl from a binary distribution (e.g., from a linux package),
you can probably get a precompiled version of this module from the same place.
Otherwise, you can get it from CPAN
(http://www.cpan.org/authors/id/G/GA/GAAS/Digest-MD5-2.33.tar.gz).
After you've downloaded it, do the following:
	gzip -dc Digest-MD5-2.33.tar.gz | tar xf -
	cd Digest-MD5-2.33
	perl Makefile.PL
	make
	make test
	make install
There is no need to reinstall makepp after installing Digest::MD5.

";
}

print "makepp successfully installed.\n";

#
# This subroutine makes a copy of an input file, substituting all occurences
# of @xyz@ with the perl variable $xyz.  It also fixes up the header line
# "#!/usr/bin/perl" if it sees one.
#
# Arguments:
# a) The input file.
# b) The output directory.
# c) The protection to give the file when it's installed.
#
sub substitute_file {
  my ($infile, $outdir, $prot) = @_;

  open(INFILE, $infile) || die "$0: can't read file $infile--$!\n";
  make_dir($outdir);

  open(OUTFILE, "> $outdir/$infile") || die "$0: can't write to $outdir/$infile--$!\n";

  while (defined($_ = <INFILE>)) {
    s@^\#!\s*(\S+?)/perl@\#!$perlbin@	# Handle #!/usr/bin/perl.
       if $. == 1;
    s/\@(\w+)\@/${$1}/g;		# Substitute anything containg @xyz@.
    if( /^#\@\@(\w+)/ ) {		# Substitute anything containg #@@xyz ... #@@
      1 until substr( <INFILE>, 0, 3 ) eq "#\@\@";
      $_ = $$1;
    }

    print OUTFILE $_;
  }
  close(OUTFILE);

  chmod $prot, "$outdir/$infile";
}

#
# Make sure a given directory exists.  Makes it and its parents if necessary.
# Arguments: the name of the directory.
#
sub make_dir {
  my $dirname = '';
  foreach (split(/\//, $_[0])) {
    if ($_ ne '') {		# Skip the top level / directory.
      $dirname .= $_;		# Make the new directory name.
      -d $dirname or
	mkdir($dirname, 0755);
    }
    $dirname .= '/';
  }
}

sub read_with_prompt {
  local $| = 1;			# Enable autoflush on STDOUT.

  print @_;			# Print the prompt.
  $_ = <STDIN>;			# Read a line.
  chomp $_;
#
# Expand environment variables and home directories.
#
  s/\$(\w+)/$ENV{$1}/g;		# Expand environment variables.
  if (s/^~(\w*)//) {		# Is there a ~ expansion to do?
    if ($1 eq '') {
      $_ = "$ENV{HOME}$_";
    } else {
      my ($name, $passwd, $uid, $gid, $quota, $comment, $gcos, $dir, $shell) = getpwnam($1);
				# Expand from the passwd file.
      if ($dir) {		# Found it?
	$_ = "$dir$_";
      } else {
	$_ = "~$1";		# Not found.  Just put the ~ back.
      }
    }
  }
  return $_;
}
