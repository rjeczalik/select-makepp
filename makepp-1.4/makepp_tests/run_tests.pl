#!/usr/bin/perl
#
# See bottom of file for documentation.
#
use Config;
use Cwd;

#
# See if this architecture defines the INT signal.
#
my $sigint;
if(defined $Config{sig_name}) {
  my $i=0;
  for(split(' ', $Config{sig_name})) {
    $sigint=$i if $_ eq 'INT';
    ++$i;
  }
}

$ENV{'PERL'} ||= $Config{'perlpath'}; # Make sure we set the $PERL variable.

$old_cwd = cwd;			# Remember where we were so we can cd back
				# here.

my $source_path;

if ($0 =~ m@/@) {		# Path specified?
  ($source_path = $0) =~ s@/([^/]+)$@@; # Get the path to our executable.
}
else {				# Find it in $PATH:
  foreach (split(/:/, $ENV{PATH}), '.') {
    my $dir = $_ || '.';	# Blank path element is .
    if (-x "$dir/$0") {
      $source_path = $dir;
      last;
    }
  }
}
$source_path or die "$0: something's wrong, can't find path to executable\n";
$source_path =~ m@^/@ or $source_path = "$old_cwd/$source_path";
				# Make path absolute.
1 while
  ($source_path =~ s@/\.(?=/|$)@@) || # Convert x/./y into x/y.
  ($source_path =~ s@/[^/]+/\.\.(?=/|$)@@); # Convert x/../y into y.

$makepp_path = $source_path;
$makepp_path =~ s@/([^/]+)$@/makepp@; # Get the path to the makepp executable,
				# which should always be in the directory
				# above us.

@ARGV or @ARGV = (glob("*.test"), glob("*.tar"), glob("*.tar.gz"));
				# Get a list of arguments.

$n_failures = 0;

# spar <http://www.cpan.org/scripts/> extraction function
# assumes DATA to be opened to the spar
sub un_spar() {
    (local $0 = $0) =~ s!.*/!!;
    my( $lines, $kind, $mode, $atime, $mtime, $name, $nl ) = (-1, 0);
    while( <DATA> ) {
	s/\r?\n$//;		# cross-plattform chomp
	if( $lines >= 0 ) {
	    print F $_, $lines ? "\n" : $nl;
	} elsif( $kind eq 'L' ) {
	    if( $mode eq 'S' ) {
		symlink $_, $name;
	    } else {
		link $_, $name;
	    }
	    $kind = 0;
	} elsif( /^###\t(?!SPAR)/ ) {
	    ($kind, $mode, $atime, $mtime, $name) = (split /\t/, $_, 6)[1..5];
	    if( !$name ) {
	    } elsif( $kind eq 'D' ) {
		-d $name or mkdir $name, 0700 or warn "$0: can't mkdir `$name': $!\n";
		$SPAR::mode{$name} = [oct( $mode ), $atime, $mtime];
	    } elsif( $kind ne 'L' ) {
		open F, ">$name" or warn "$0: can't open >`$name': $!\n";
		$lines = abs $kind;
		$nl = ($kind < 0) ? '' : "\n";
	    }
	} elsif( defined $mode ) {
	    warn "$0: $archive:$.: trailing garbage ignored\n";
	}			# else before beginning of spar
    } continue {
	if( !$lines-- ) {
	    close F;
	    chmod oct( $mode ), $name;
	    utime $atime, $mtime, $name;
	}
    }

    for( keys %SPAR::mode ) {
	chmod shift @{$SPAR::mode{$_}}, $_;
	utime @{$SPAR::mode{$_}}, $_;
    }
    %SPAR::mode = ();
}

test_loop:
foreach $tarfile (@ARGV) {
  my $testname;
  ($testname = $tarfile) =~ s/\..*$//; # Test name is tar file name w/o extension.
  if ($tarfile !~ /^\//) {	# Not an absolute path to tar file?
    $tarfile = "$old_cwd/$tarfile"; # Make it absolute, because we're going
  }				# to cd below.

  my $tarcmd;
#  if ($testname =~ /\.tar$/) {	# Tar file
#    $tarcmd = "tar xf $tarfile";
#  }
  if ($testname =~ /\.gz$/) { # Compressed tar file?
    $tarcmd = "gzip -dc $tarfile | tar xf -";
  }
  elsif ($testname =~ /\.bz2$/) { # Tar file compressed harder?
    $tarcmd = "bzip2 -dc $tarfile | tar xf -";
  }
  -d "tdir" and system_intabort("rm -rf tdir"); # Get rid of junk from previous run.
  -d "$testname.failed" and system_intabort("chmod -R u+rwx $testname.failed; rm -rf $testname.failed");
  mkdir "tdir", 0755 || die "$0: can't make directory tdir--$!\n";
				# Make a directory.
  chdir "tdir" || die "$0: can't cd into tdir--$!\n";

  my $log="$testname.log";
  $log="../$log" if $log !~ /^\//;
  eval {
    $^O eq 'cygwin' && $testname =~ /_unix/ and die "skipped\n";
				# Skip things that will cause errors on cygwin.
				# E.g., the test for file names with special
				# characters doesn't work under NT!
    if( $tarcmd ) {
      system_intabort($tarcmd) && # Extract the tar file.
	die "$0: can't extract testfile $tarfile\n";
    } else {
      open DATA, $tarfile or die "$0: can't open $tarfile--$!\n";
      un_spar;
    }
    if (-x "is_relevant") {
      system_intabort("./is_relevant $makepp_path $^O > /dev/null 2>&1") && die "skipped\n";
    }
    if (-x "makepp_test_script") { # Shell script to execute?
      system_intabort("./makepp_test_script $makepp_path > $log 2>&1") && # Run it.
	die "$0: makepp returned error status\n";
    } else {
      system_intabort("\$PERL $makepp_path > $log 2>&1") && # Run makepp.
	die "$0: makepp returned error status\n";
    }
#
# Now look at all the final targets:
#
    use File::Find;
    find sub {
	return if $_ eq 'n_files'; # Skip the special file.
	return if -d;		   # Skip subdirectories, find recurses.
	local $/ = undef;		# Slurp in the whole file at once.
	open TFILE, $_ or die "$0: can't open tdir/$File::Find::name--$!\n";
	$tfile_contents = <TFILE>; # Read in the whole thing.
	$tfile_contents =~ s/\r//g; # For cygwin, strip out the extra CRs.

	# Get the name of the actual file, older find can't do no_chdir.
	($mtfile = $File::Find::name) =~ s!answers/!!;
	open MTFILE, "$old_cwd/tdir/$mtfile" or die "$mtfile\n";
	my $mtfile_contents = <MTFILE>; # Read in the whole file.
	$mtfile_contents =~ s/\r//g; # For cygwin, strip out the extra CRs.
	$mtfile_contents eq $tfile_contents or die "$mtfile\n";
    }, 'answers';
    close TFILE;
    close MTFILE;

#
# See if the correct number of files were built:
#
    my $n_files_updated;
    open(LOGFILE, ".makepp_log") or die ".makepp_log\n";
    while (defined($_ = <LOGFILE>)) {
      if (/^(\d+) files updated/) {
	$n_files_updated = $1;	# Found the count of files changed.
	last;
      }
    }
    close LOGFILE;
    rename ".makepp_log" => ".makepp_log.failed";
				# Get rid of the log file so we don't
				# get confused if the next test doesn't make
				# a log file for some reason.
    defined($n_files_updated) or die ".makepp_log\n";

    if (open(N_FILES, "answers/n_files")) { # Count of # of files updated?
      $_ = <N_FILES>;
      close N_FILES;
      /^(\d+)/ or die "answers/n_files\n";
      $1 == $n_files_updated or die "n_files\n";
    }

#
# Also search through the log file to make sure there are no perl messages
# like "uninitialized value" or something like that.
#
    if (open LOGFILE, $log) {
      while (defined($_ = <LOGFILE>)) {
	/at \S+ line \d+/ and die "$log\n";
	/generated error/ and die "$log\n";
      }
    }
    close LOGFILE;
  };

  if ($@) {
    chdir $old_cwd;		# Get back to the old directory.
    if ($@ =~ /skipped/) {	# Skip this test?
      print "skipped $testname\n";
      next;
    } elsif ($@ =~ /^\S+$/) {	# Just one word?
      my $loc = $@;
      $loc =~ s/\n//;		# Strip off the trailing newline.
      print "FAILED $testname (at $loc)\n";
    } else {
      print "FAILED $testname: $@";
    }
    ++$n_failures;
    close TFILE; close MTFILE;	# or cygwin will hang
    rename tdir => "$testname.failed";
  } else {
    print "passed $testname\n";
    -x "cleanup_script" and system_intabort("./cleanup_script >> $log 2>&1");
    chdir $old_cwd;		# Get back to the old directory.
    system_intabort( 'rm -rf tdir' ); # Get rid of the test directory.
  }
}

exit $n_failures;

#
# Equivalent of system() except that it handles INT signals correctly.
#
sub system_intabort {
  system @_;
  kill 'INT', $$ if $sigint && $?==$sigint;
  return $?;
}


=head1 NAME

run_tests.pl -- Run makepp regression tests

=head1 SYNOPSYS

  run_tests.pl test1.test test2.test

If no arguments are specified, defaults to *.test.

=head1 DESCRIPTION

This script runs the specified tests and reports their result.

A test is stored as a file with an extension of F<.test> (old format), or
F<.tar> or F<.tar.gz>.	A file with an extension of F<.test> is really
just a tar file in disguise.

First a directory is created called F<tdir> (called the test directory
below).	 Then we cd to the directory, then extract the contents of the
tar file.  This means that the tar file ought to contain top-level
files, i.e., it should contain F<./Makeppfile>, not F<tdir/Makeppfile>.

The following files within this directory are important:

=over 4

=item is_relevant

If this file exists, it should be a shell script which returns true (0)
if this test is relevant on this platform, and false (nonzero) if the
test is not relevant.

The first argument to this script is the full path of the makepp executable we
are testing.  The second argument is the current platform as seen by Perl.
The environment variable C<PERL> is the path to the perl executable we are
supposed to use (which is not necessarily the one in the path).

=item makepp_test_script

If this file exists, it should be a shell script which runs makepp after
setting up whatever is necessary.  If this script returns false (a
non-zero value), then the test fails.

The first argument to this script is the full path of the makepp
executable we are testing.  The environment variable C<PERL> is the path
to the perl executable we are supposed to use (which is not necessarily
the one in the path).

This script must be sufficiently generic to work in all test
environments.  For example:

=over 4

=item *

It must not assume that perl is in the path.  Always use $PERL instead.

=item *

It must work with the Bourne shell, i.e., it may contain no bash
extensions.

=item *

It must not use "echo -n" because that doesn't work on HP machines.

=back

If this file does not exist, then we simply execute the command
S<C<$PERL makepp>>, so makepp builds all the default targets in the makefile.

=item F<Makefile> or F<Makeppfile>

Obviously this is kind of important.

=item answers

This directory says what the result should be after running the test.
Each file in the answers directory, or any of its subdirectories, is
compared to a file of the same name in the test directory (or its
corresponding subdirectory).  The files must be exactly identical or the
test fails.

Files in the main test directory do not have to exist in the F<answers>
subdirectory; if not, their contents are not compared.

There is one special file in the F<answers> subdirectory: the file
F<answers/n_files> should contain an integer in ASCII format which is
the number of files that makepp ought to build.	 This is compared to the
corresponding number of files that it actually built, extracted from the
logfile F<.makepp_log>.

=item cleanup_script

If this file exists, it should be a shell script that is executed when
the test is done.  This script is executed just before the test
directory is deleted.  No cleanup script is necessary if the test
directory and all the byproducts of the test can be deleted with just
S<C<rm -rf tdir>>.  (This is usually the case, so most tests don't
include a cleanup script.)

=back

=cut
