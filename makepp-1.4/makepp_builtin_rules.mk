# $Id: makepp_builtin_rules.mk,v 1.10 2004/02/27 07:53:16 grholt Exp $
# Please do NOT use this as an example of how to write a makefile.  This is
# NOT A typical makefile.
#
# These are the rules that are built into makepp, that it can apply without
# anything in the makefile.  Don't put too much junk in here!  One problem
# with GNU make that I ran into was that its default rules included stuff for
# dealing with very rare suffixes that I happened to use in a different
# and incompatible way.
#
# This file is read in after reading all of each makefile.
#
# It's not a good idea to put definitions of standard variables like CC, etc.,
# in here, because then (a) the $(origin ) function doesn't work; (b) the 
# values won't be visible except in rules, because this file is loaded after
# everything else in the makefile is processed.  Standard variables are 
# implemented as functions that have no arguments (see Makesubs.pm).
#

#
# Rules.  Special code in makepp makes it so these rules never override any 
# kind of rule that is contained in a user makeppfile.
#
_CPP_SUFFIXES := cxx c++ cc cpp C
perl_begin
if ($^O eq 'cygwin') {
  $_IS_CYGWIN = 1;
  $_CPP_SUFFIXES = "cxx c++ cc cpp";
				# Uppercase C as a suffix is indistinguishable
				# from a lowercase C on a case-insensitive
  				# file system.
  $_EXE_SUFFIX = ".exe";
}

perl_end

ifneq $(_IS_CYGWIN)
#
# We want "makepp xyz" to make xyz.exe if this is cygwin.
# This is a hack hack hack to make a phony target xyz that depends on
# xyz.exe.  We must mark xyz as a phony target *after* we have associated
# a rule with the target, or else the rule will not work because makepp
# specifically rejects builtin rules for phony targets (to prevent disasters).
# (See code in set_rule().)  So we evaluate $(phony ) only after the
# rule has been set.  This kind of shenanigan is never necessary in normal
# makefiles because there are no special restrictions about rules from anywhere
# except this file.
#
$(basename $(foreach)): $(foreach) : foreach *.exe
	@: $(phony $(output))
endif

#
# Link command:
#
$(basename $(foreach))$(_EXE_SUFFIX) : $(infer_objects $(foreach), *.o) : foreach *.o
	$(infer_linker $(inputs)) $(inputs) $(LDLIBS) $(LDFLAGS) $(LIBS) -o $(output)
	noecho perl {{
	  if (f_target eq 'test') {
	    print "Warning: on unix, to run a program called 'test', you usually must type\n";
	    print "  ./test\n";
	    print "rather than just 'test'.\n";
	  }
	}}

#
# C++ compilation:
#
ifneq $(percent_subdirs)
$(basename $(foreach)).o : $(foreach) : foreach **/*.$(_CPP_SUFFIXES)
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) -c $(input) -o $(output)
else
$(basename $(foreach)).o : $(foreach) : foreach *.$(_CPP_SUFFIXES)
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) -c $(input) -o $(output)
endif

#
# C compilation:
#
%.o: %.c
	$(CC) $(CFLAGS) $(CPPFLAGS) -c $(input) -o $(output)

#
# Fortran compilation:
#
%.o: %.f
	$(FC) $(FFLAGS) $(CPPFLAGS) -c $(input) -o $(output)

#
# The rules for yacc and lex are marked :signature target_newer because we
# don't want to reexecute them if the file already exists.  Some systems don't
# have yacc or lex installed, and some makefiles (e.g., the KDE makefiles)
# don't have a proper yacc/lex rule but run yacc/lex as part of the action of
# a phony target.
#

#
# Yacc:
#
%.c: %.y
	: signature target_newer
	$(YACC) $(YFLAGS) $(input)
	perl { rename 'y.tab.c', f_output or die "$!\n" }

#
# Lex:
#
%.c: %.l
	: signature target_newer
	$(LEX) $(LFLAGS) -t $(input)
	perl { rename 'lex.yy.c', f_output or die "$!\n" }
