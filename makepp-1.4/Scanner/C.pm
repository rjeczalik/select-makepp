=head1 NAME

Scanner::C - makepp scanner for C files

=head1 DESCRIPTION

Scans a C file for C<#include>'s.

Tags are:

=over 6

=item user

File scanned due to an #include "filename" directive.

=item sys

File scanned due to an #include E<lt>filenameE<gt> directive.

=back

=cut

use strict;
package Scanner::C;

use Scanner;
use vars qw/@ISA/;
BEGIN { @ISA = qw/Scanner/; }

sub get_macro {
	if(/\G(\`*)([a-z_]\w*)/igc) {
		return ($1, "", $2);
	}
	return;
}

sub expand_macros {
	my $self = shift;
	my ($expr, $visited) = @_;
	$visited ||= {};
	local $_ = $expr;
	$expr = "";
	pos($_) = 0;

	# TBD: Deal with macros that have parameters, as well as the
	# defined() built-in.

	while(1) {
		last if length($_) <= pos($_);
		if(/\G(\\.)/sgc) {
			$expr .= $1;
		}
		elsif(/\G(\"(?:[^"]*\\\")*[^"]*\")/gc) {
			$expr .= $1;
		}
		elsif(my ($prefix, $key_prefix, $key) = $self->get_macro) {
			$expr .= $prefix;
			my $x=$self->get_var($key) unless $visited->{$key};
			if(defined $x) {
			  	my %v=%$visited;
				$v{$key}=1;
				# NOTE: pos() isn't preserved by local:
				my $pos = pos($_);
			  	$expr .= $self->expand_macros($x, \%v);
				pos($_) = $pos;
			}
			else {
			  	$expr .= $key_prefix . $key;
			}
		}
		else {
			/\G([^\\"a-z_`]+)/igc or die;
			$expr .= $1;
		}
	}
	return $expr;
}

sub get_directive {
	if(s/^\s*\#\s*(\w+)\s+//) {
		return $1;
	}
	return;
}

sub xscan_file {
	my $self=shift;
	my ($cp, undef, $finfo, $conditional, $fh)=@_;
	my $absname=$finfo->absolute_filename;
	while(<$fh>) {
		if(my $directive = $self->get_directive) {
			s|(/\*.*?\*/\s*)+$||;
			s|\s*(//.*)?$||;
			if($directive eq "include") {
				$_ = $self->expand_macros($_) if $conditional;
				if(s/^\"([^"]*)\"//) {
					$self->include($cp, "user", $1, $finfo)
					  or return undef;
				}
				elsif(s/^\<([^>]*)\>//) {
					$self->include($cp, "sys", $1, $finfo)
					  or return undef;
				}
				warn "Ignoring trailing cruft \"$_\"" if /\S/;
			}
			elsif($conditional) {
				if($directive eq "define" &&
				  /^(\w+)\s*(.*?)$/
				) {
					$self->set_var($1, $2);
				}
				elsif($directive eq "undef" && 
				  /^(\w+)$/
				) {
					$self->set_var($1, undef);
				}
				elsif($directive=~/^if(n)?def$/ &&
				  /^(\w+)$/
				) {
					my $def=(defined $self->get_var($1));
					my $no=($directive eq "ifndef");
					$self->push_scope($no ? !$def : $def);
				}
				elsif($directive eq "else") {
					$self->push_scope(!$self->pop_scope);
					warn "Ignoring trailing cruft \"$_\""
					  if /\S/;
				}
				elsif($directive eq "endif") {
					$self->pop_scope();
					warn "Ignoring trailing cruft \"$_\""
					  if /\S/;
				}
				elsif(
				  $directive eq "if" || $directive eq "elif"
				) {
					my $act = $self->pop_scope
					  if $directive eq "elif";
					$_ = $self->expand_macros($_);
					my $go;
					if($act) {
						$go = 0;
					}
					elsif(
					  /^([1-9]\d*|[0-7]+|0x[\da-f]+)\s*$/i
					) {
						$go = eval $_;
					}
					else {
			 # TBD: We could do fancier things here to evaluate
			 # "#if" expressions, but the main goal is to handle
			 # "#if 0" and "#if 1" correctly.
						$go = 1;
						chomp;
						warn
 "Assuming that complex #if expression \"$_\" is true";
					}
					$self->push_scope($go);
				}
			}
		}
	}
	return 1;
}

1;
