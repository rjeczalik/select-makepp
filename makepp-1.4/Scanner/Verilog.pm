=head1 NAME

Scanner::Verilog - makepp scanner for Verilog files

=head1 DESCRIPTION

Scans a verilog file for C<`include>'s and instances.

Tags are:

=over 6

=item vinc

File scanned due to an `include directive or specified on the command line.

=item vlog

File scanned due to a requested instance.

=item vlib

File scanned due to a -v option.

=back

=cut

use strict;
package Scanner::Verilog;

use Scanner;
use vars qw/@ISA/;
BEGIN { @ISA = qw/Scanner/; }

sub new {
	my $self=shift;
	my $class=ref($self) || $self;
	$self=$class->SUPER::new(@_);

	# MODULE_MAP maps a module to the list of modules it instantiates
	$self->{MODULE_MAP}={};

	# RESOLVED maps modules that have been resolved to 1
	$self->{RESOLVED}={};

	# UNRESOLVED maps unresolved modules that have been instanced to 1
	$self->{UNRESOLVED}={};

        # MODULE is the name of the current module being parsed, if any.
        # Although this seems like it ought to be tied to a each file, in
        # reality the real VCS parser is flat, meaning that it treats the
        # input files as though they had been concatenated into a single input
        # file.
        $self->{MODULE}=undef;

	return bless $self, $class;
}

sub info_string {
	die "info_string doesn't work with Scanner::Verilog";
}

# Always scan, because Verilog compilers (typically) also do linking, and
# therefore we need to look for module definitions even if they're stable.
sub dont_scan {
	return 0;
}

sub resolve_module {
	my $self=shift;
	my ($cp, $module, $finfo, $visited)=@_;
	$visited ||= {};
	return 1 if $self->{RESOLVED}{$module};
	my $map=$self->{MODULE_MAP}{$module};
	#main::print_log("RESOLVING: $module");
	if($map) {
		my ($nfinfo, @map)=@$map;
		for(@map) {
			if($visited->{$_}) {
				main::print_warning("module $_ contains itself");
			}
			else {
				my $v={%$visited};
				$v->{$_}=1;
				$self->resolve_module($cp, $_, $nfinfo, $v)
				  or return undef;
			}
		}
	}
	else {
		$self->include($cp, "vlog", $module, $finfo) or return undef;
	}
	$self->{RESOLVED}{$module}=1;
	#main::print_log("RESOLVED: $module");
	return 1;
}

sub resolve {
	my $self=shift;
	my ($cp)=@_;
	while(my @keys=keys %{$self->{UNRESOLVED}}) {
		my $key=shift(@keys);
		$self->resolve_module($cp, $key, $self->{UNRESOLVED}{$key})
		  or return undef;
		delete $self->{UNRESOLVED}{$key};
	}
	return 1;
}

my @primitives = qw{
  and always assign attribute begin buf bufif0 bufif1 case cmos deassign
  default defparam disable else endattribute end endtable endtask event
  for force forever fork function highz0 highz1 if initial inout input
  integer join large medium nand negedge nor not notif0 notif1 nmos or
  output parameter pmos posedge pulldown pullup pull0 pull1 rcmos reg release
  repeat rnmos rpmos rtran rtranif0 rtranif1 scalared small specify specparam
  strong0 strong1 supply0 supply1 table task tran tranif0 tranif1 time
  tri triand trior trireg tri0 tri1 vertored wait wand weak0 weak1 while
  wire wor xor xnor
};
my %is_prim;
@is_prim{@primitives} = (1..@primitives);

sub xscan_file {
	my $self=shift;
	my ($cp, $tag, $finfo, $conditional, $fh)=@_;
	my $absname=$finfo->absolute_filename;
	my $module = $self->{MODULE};
	my $in_comment;
	my $pend_imodule;
	while(<$fh>) {
		my $directive;
		my $pend_imodule_next;
		if($in_comment) {
			undef $in_comment if s@^.*?\*/@@;
		}
		# NOTE: If there are overlapping "//" and "/*" comments, then
		# the one that was introduced first wins. For example,
		# "/* // */ foo" produces " foo", and "// /*" does *not* set
		# $in_comment.
		s@(:?//.*|/\*.*?\*/)@@g;
		my $in_comment_next = $in_comment || s@/\*.*$@@;
		if($in_comment) {
			# Do nothing.
		}
		elsif(s/^\s*\`\s*(\w+)\s+//) {
			$directive=$1;
			if($directive eq "include" && /^\"([^"]*)\"/) {
				$self->include($cp, "vinc", $1, $finfo)
				  or return;
			}
			elsif($conditional) {
				if($directive eq "define" &&
				  /^(\w+)\s*(.*?)\s*$/
				) {
					$self->set_var($1, $2);
				}
				elsif($directive eq "undef" && 
				  /^(\w+)\s*$/
				) {
					$self->set_var($1, undef);
				}
				elsif($directive=~/^if(n)?def$/ &&
				  /^(\w+)\s*$/
				) {
					my $def=(defined $self->get_var($1));
					my $no=($directive eq "ifndef");
					$self->push_scope($no ? !$def : $def);
				}
				elsif($directive eq "else") {
					$self->push_scope(!$self->pop_scope);
				}
				elsif($directive eq "endif") {
					$self->pop_scope();
				}
			}
		}
		elsif($self->is_active) {
			my $imodule; # TBD: Scope should be smaller
			if(/^\s*(?:module|primitive)\s+(\w+)/) {
				main::print_warning(
 "no closing endmodule for module $module in `$absname:$.'"
				) if $module;
				$module=$1;
                                $self->{MODULE} = $module if $conditional;
				my $map=$self->{MODULE_MAP};
				if($map->{$module}) {
					my $n2 =
 $map->{$module}[0]->absolute_filename;
					main::print_warning(
 "module $module multiply defined in `$absname' and `$n2'"
					);
				}
				$map->{$module}=[$finfo];
			}
			elsif(m!^\s*end(?:module|primitive)\s*$!) {
				undef $module;
                                $self->{MODULE} = undef if $conditional;
			}
			elsif(
                          ($imodule) = /^\s*(\w+)(?:\s+\#\(.*\))?\s+\w+\s*\(/ or
                          ($imodule = $pend_imodule and /^\s*\(/)
                        ) { # balance ))
				if($is_prim{$imodule}) {
					# Do nothing
				}
				elsif($module) {
					#main::print_log("INSTANCE: $imodule IN $module");
					push(@{$self->{MODULE_MAP}{$module}},
					  $imodule
					);
					$self->{UNRESOLVED}{$imodule}=$finfo
					  unless $tag eq "vlib" ||
					  $self->{RESOLVED}{$imodule};
				}
				else {
					main::print_warning(
 "instance of $imodule not enclosed in a module in `$absname:$.'"
					);
				}
			}
			elsif(/^\s*(\w+)(?:\s+\#\(.*\))?\s+\w+\s*$/) {
				$pend_imodule_next = $1;
			}
                        elsif(/^\s*$/) {
				$pend_imodule_next = $pend_imodule;
                        }
		}
		$in_comment = $in_comment_next;
		$pend_imodule = $pend_imodule_next;
	}
	main::print_warning(
 "no closing endmodule for module $module in `$absname'"
	) if $module && $tag ne "vinc";
	return 1;
}

=head1 FUTURE WORK

I'm not sure whether a module that is defined by a file `include'ed by
a B<-v> file has its instances resolved immediately (the current scanner
implementation), or whether it's deferred.
In the latter case, we would need to add a new "vlibinc" tag to model this.
On the other hand, this is a pretty weird case that probably never occurs
in practice, and arguably you deserve what you get if you do it.

=cut

1;
